package org.unicen.externaljar.listener;

import org.unicen.abenchmark.core.test.TestRun;
import org.unicen.abenchmark.support.TestListenerSupport;

/**
 * Created by mgiorda on 2/12/17.
 */
public class SampleTestListener extends TestListenerSupport {

    @Override
    public void onTestFinish(TestRun testRun) {

        System.out.println("Test has finished!");
    }
}
