package org.unicen.externaljar;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.unicen.abenchmark.core.operation.Operation;

import java.io.IOException;
import java.util.Objects;

public class WebServiceOperation implements Operation<Void> {

    private final OkHttpClient client = new OkHttpClient();
    private final Request request;

    public WebServiceOperation(Request request) {

        Objects.requireNonNull(request, "request cannot be null");

        this.request = request;
    }

    @Override
	public Void execute() {
		
        Request request = new Request.Builder()
                .url("http://www.google.com")
                .build();


        try {
            client.newCall(request).execute();
            System.out.println("Ejecutando web service desde jar externo");

        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return null;
	}
}
