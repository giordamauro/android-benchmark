package org.unicen.externaljar;

import org.unicen.abenchmark.annotation.Benchmark;
import org.unicen.abenchmark.annotation.Evaluator;
import org.unicen.abenchmark.annotation.TestListeners;
import org.unicen.abenchmark.annotation.WarmUp;
import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.support.evaluators.OnlyOnce;
import org.unicen.externaljar.listener.SampleTestListener;

@Benchmark
@WarmUp(iterations = 5)
@Evaluator(OnlyOnce.class)
@TestListeners(SampleTestListener.class)
public class AnnotationTestOperation implements Operation<Void> {

    @Override
	public Void execute() {


        System.out.println("Annotation test execution");

		return null;
	}
}
