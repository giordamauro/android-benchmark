package org.unicen.abenchmark.api.model.device;

import lombok.*;
import lombok.experimental.Tolerate;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

/**
 * Created by Mauro on 16/07/2016.
 */
@Value
@Builder
public class AndroidVersion {

    @NotNull
    String release;

    @NotNull
    Integer sdkInt;

    String baseOs;
    String codename;
    String incremental;
    String securityPatch;

    @Tolerate
    private AndroidVersion() {

        this.release = null;
        this.sdkInt = null;
        this.baseOs = null;
        this.codename = null;
        this.incremental = null;
        this.securityPatch = null;
    }
}