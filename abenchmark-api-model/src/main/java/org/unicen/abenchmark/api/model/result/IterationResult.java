package org.unicen.abenchmark.api.model.result;

import lombok.*;
import lombok.experimental.Tolerate;

import javax.validation.constraints.NotNull;
import java.util.List;


@Value
@Builder
public class IterationResult {

    @NotNull
    Long startTime;

    @NotNull
    Long endTime;

    String resultType;

    Object result;

    List<Metric> metrics;

    @Tolerate
    private IterationResult() {

        this.startTime = null;
        this.endTime = null;
        this.result = null;
        this.resultType = null;
        this.metrics = null;
    }
}
