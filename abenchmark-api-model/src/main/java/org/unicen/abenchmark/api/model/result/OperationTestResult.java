package org.unicen.abenchmark.api.model.result;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.Tolerate;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.util.List;


@Value
@Builder
public class OperationTestResult {

    @NotNull
    String operationType;

    @NotNull
    Long testTime;

    List<Metric> testMetrics;

    List<Metric> operationMetrics;


    @Tolerate
    private OperationTestResult() {

        this.operationType = null;
        this.testTime = null;
        this.testMetrics = null;
        this.operationMetrics = null;
    }
}
