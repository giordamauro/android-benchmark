package org.unicen.abenchmark.api.model.result;

import lombok.*;
import lombok.experimental.NonFinal;
import lombok.experimental.Tolerate;
import org.springframework.data.annotation.Id;
import org.unicen.abenchmark.api.model.test.Test;

import javax.validation.constraints.NotNull;
import java.util.List;


@Value
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class TestResult {

    @Id
    @NonFinal
    String id;

    @NotNull
    String testId;

    @NotNull
    List<OperationTestResult> operations;
}
