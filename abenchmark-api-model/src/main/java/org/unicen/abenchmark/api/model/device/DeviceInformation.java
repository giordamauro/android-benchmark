package org.unicen.abenchmark.api.model.device;

import lombok.*;
import lombok.experimental.Tolerate;

import javax.validation.constraints.NotNull;


/**
 * Created by Mauro on 16/07/2016.
 */
@Value
@Builder
public class DeviceInformation {

    @NotNull
    AndroidVersion version;

    @NotNull
    String brand;

    @NotNull
    String manufacturer;

    @NotNull
    String model;

    String board;
    String bootloader;
    String device;
    String display;
    String fingerprint;
    String hardware;
    String host;
    String id;
    String product;
    String serial;
    String tags;
    String type;
    String unknown;
    String user;

    @Tolerate
    private DeviceInformation() {

        this.version = null;
        this.board = null;
        this.bootloader = null;
        this.brand = null;
        this.device = null;
        this.display = null;
        this.fingerprint = null;
        this.hardware = null;
        this.model = null;
        this.host = null;
        this.id = null;
        this.product = null;
        this.serial = null;
        this.tags = null;
        this.type = null;
        this.unknown = null;
        this.user = null;
        this.manufacturer = null;
    }
}