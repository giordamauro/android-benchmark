package org.unicen.abenchmark.api.model.file;

import lombok.*;
import lombok.experimental.NonFinal;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;


@Value
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class JarFile {

    @Id
    @NonFinal
    String id;

    @NotNull
    String name;

    @NotNull
    String jar;

    @NonFinal
    @Setter
    String dex;
}
