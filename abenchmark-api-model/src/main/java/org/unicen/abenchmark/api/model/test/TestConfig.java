package org.unicen.abenchmark.api.model.test;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.Tolerate;
import org.unicen.abenchmark.context.model.Bean;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Created by Mauro on 16/07/2016.
 */
@Value
@Builder
public class TestConfig {

    @NotNull
    @Valid
    Bean operation;

    @Valid
    Bean evaluator;

    Integer warmUpIterations;

    List<Bean> operationListeners;
    List<Bean> testListeners;

    @Tolerate
    private TestConfig() {

        this.operation = null;
        this.warmUpIterations = null;
        this.operationListeners = null;
        this.testListeners = null;
        this.evaluator = null;
    }
}