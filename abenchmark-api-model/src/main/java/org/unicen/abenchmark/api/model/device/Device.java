package org.unicen.abenchmark.api.model.device;

import lombok.*;
import lombok.experimental.NonFinal;
import lombok.experimental.Tolerate;
import org.springframework.data.annotation.Id;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Value
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Device {

    @Id
    @NonFinal
    String id;

    /**
     * This field should be unique
     */
    @Valid
    @NotNull
    String deviceUuid;

    @Valid
    @NotNull
    DeviceInformation information;
}
