package org.unicen.abenchmark.api.model.test;

import lombok.*;
import lombok.experimental.NonFinal;
import lombok.experimental.Tolerate;
import org.springframework.data.annotation.Id;
import org.unicen.abenchmark.api.model.device.AndroidVersion;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Created by Mauro on 16/07/2016.
 */
@Value
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Test {

    @Id
    @NonFinal
    String id;

    @NotNull
    String deviceUuid;

    @NonFinal
    @Setter
    TestStatus status;

    List<String> sources;

    @Valid
    List<TestConfig> testConfigs;

    @NonFinal
    @Setter
    List<String> testClasses;
}