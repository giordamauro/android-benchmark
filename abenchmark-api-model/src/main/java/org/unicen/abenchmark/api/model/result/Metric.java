package org.unicen.abenchmark.api.model.result;

import lombok.*;

import javax.validation.constraints.NotNull;


@Value
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Metric {

    @NotNull
    String name;

    @NotNull
    String type;

    @NotNull
    Object value;
}
