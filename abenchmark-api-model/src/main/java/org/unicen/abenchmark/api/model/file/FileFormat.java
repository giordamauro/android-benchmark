package org.unicen.abenchmark.api.model.file;

/**
 * Created by mgiorda on 2/4/17.
 */
public enum FileFormat {

    JAR(".jar"), DEX(".dex");

    private final String extension;

    FileFormat(String extension){
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }
}
