package org.unicen.abenchmark.api.model.test;

/**
 * Created by mgiorda on 11/28/16.
 */
public enum TestStatus {

    PENDING, RUNNING, DONE
}
