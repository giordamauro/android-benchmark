package org.unicen.abenchmark.context.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

class BeanContainer {

    private final Map<String, Object> instancesById = new HashMap<>();
    private final Map<Class<?>, Object> instancesByClass = new HashMap<>();

    public void registerBeanInstance(Object beanInstance, String beanId) {

        Objects.requireNonNull(beanInstance, "BeanInstance cannot be null");

        if (beanId != null) {

            if (instancesById.containsKey(beanId)) {
                throw new IllegalStateException(String.format("Bean id %s is not unique", beanId));
            }
            instancesById.put(beanId, beanInstance);
        }

        Class<?> beanClass = beanInstance.getClass();
        if (instancesByClass.containsKey(beanClass)) {
            throw new IllegalStateException(String.format("Bean class %s is not unique", beanClass.getName()));
        }
        instancesByClass.put(beanClass, beanInstance);
    }

    public Object getBeanById(String ref) {

        Objects.requireNonNull(ref, "BeanId cannot be null");
        Object refInstance = instancesById.get(ref);

        if (refInstance == null) {
            throw new IllegalStateException("Cannot find reference: " + ref);
        }

        return refInstance;
    }

    public <T> T getBeanByClass(Class<T> beanType) {

        Objects.requireNonNull(beanType, "BeanType cannot be null");
        Object beanInstance = instancesByClass.get(beanType);

        if (beanInstance == null) {
            throw new IllegalStateException("Cannot find beanType: " + beanType);
        }

        @SuppressWarnings("unchecked")
        T castedInstance = (T) beanInstance;

        return castedInstance;
    }
}
