package org.unicen.abenchmark.context.model;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ToString
public class FactoryBean {

    private final String ref;

    @NotNull
    private final String method;

    private final Bean bean;

    private FactoryBean() {

        this.ref = null;
        this.method = null;
        this.bean = null;
    }
}
