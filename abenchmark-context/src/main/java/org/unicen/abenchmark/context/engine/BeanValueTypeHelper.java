package org.unicen.abenchmark.context.engine;

import org.unicen.abenchmark.context.model.Bean;
import org.unicen.abenchmark.context.model.BeanDeclaration;
import org.unicen.abenchmark.context.model.BeanProperty;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

class BeanValueTypeHelper {

    private final BeanContainer beanContainer;
    private final BeanFactory beanFactory;
    private final PropertiesContainer propertiesContainer;
    private final ReflectionHelper reflectionHelper;

    BeanValueTypeHelper(BeanContainer beanContainer, BeanFactory beanFactory,
                        PropertiesContainer propertiesContainer, ReflectionHelper reflectionHelper) {

        this.beanContainer = beanContainer;
        this.beanFactory = beanFactory;
        this.propertiesContainer = propertiesContainer;
        this.reflectionHelper = reflectionHelper;
    }

    public void addBeanProperties(Object beanInstance, List<BeanProperty> properties) {

        Objects.requireNonNull(beanInstance, "BeanInstance cannot be null");

        if (properties != null) {

            for (BeanProperty property : properties) {

                if (property.getName() == null) {
                    throw new IllegalStateException("BeanProperty name cannot be null");
                }

                ValueType valueType = this.getByDeclaration(property);

                Method setterFieldMethod = this.getSetterFieldMethod(beanInstance.getClass(), property.getName(), valueType.getType());

                if(setterFieldMethod != null) {
                    reflectionHelper.invokeMethod(setterFieldMethod, beanInstance, valueType.getValue());
                }
                else {
                    reflectionHelper.setFieldValue(beanInstance, property.getName(), valueType.getValue());
                }
            }
        }
    }

    public List<ValueType> getByDeclarationList(List<BeanDeclaration> declarationList) {

        List<ValueType> instanceList = new ArrayList<>();

        for (BeanDeclaration contructorArg : declarationList) {

            ValueType valueType = this.getByDeclaration(contructorArg);
            instanceList.add(valueType);
        }

        return instanceList;
    }

    private ValueType getByDeclaration(BeanDeclaration declaration) {

        ValueType valueType;

        if (declaration.getValue() != null) {

            String value = declaration.getValue();
            String type = declaration.getType();

            valueType = getValueType(value, type);
        } else {
            Object beanInstance;

            if (declaration.getRef() != null) {

                String ref = declaration.getRef();
                beanInstance = beanContainer.getBeanById(ref);
            } else if (declaration.getBean() != null) {

                Bean bean = declaration.getBean();
                beanInstance = beanFactory.createBean(bean);

            } else if (declaration.getList() != null) {

                List<ValueType> listValues = this.getByDeclarationList(declaration.getList());
                beanInstance = this.getValueTypesValues(listValues);

            } else {
                throw new IllegalStateException("Ref, Value, List or Bean must be set");
            }

            if (beanInstance == null) {
                throw new IllegalStateException("Bean instance cannot be null");
            }

            valueType = new ValueType(beanInstance.getClass(), beanInstance);
        }

        return valueType;
    }

    private ValueType getValueType(String value, String type) {

        String propertyValue = propertiesContainer.getPropertyValue(value);
        Class<?> valueType = reflectionHelper.getTypeClassForName(type);

        Object argValue = propertyValue;

        if (valueType.isAssignableFrom(Integer.class) || valueType.isAssignableFrom(int.class)) {
            argValue = Integer.valueOf(propertyValue);
        } else if (valueType.isAssignableFrom(Boolean.class) || valueType.isAssignableFrom(boolean.class)) {
            argValue = Boolean.valueOf(propertyValue);
        } else if (valueType.isAssignableFrom(Long.class) || valueType.isAssignableFrom(long.class)) {
            argValue = Long.valueOf(propertyValue);
        } else if (valueType.isAssignableFrom(Double.class) || valueType.isAssignableFrom(double.class)) {
            argValue = Double.valueOf(propertyValue);
        }

        return new ValueType(valueType, argValue);
    }

    public List<Object> getValueTypesValues(List<ValueType> listValues) {

        List<Object> values = new ArrayList<>();

        for (ValueType listValueType : listValues) {
            Object value = listValueType.getValue();
            values.add(value);
        }

        return values;
    }

    public List<Class<?>> getValueTypesTypes(List<ValueType> listValues) {

        List<Class<?>> types = new ArrayList<>();

        for (ValueType listValueType : listValues) {
            Class<?> type = listValueType.getType();
            types.add(type);
        }

        return types;
    }

    public Method getSetterFieldMethod(Class<?> beanClass, String fieldName, Class<?> fieldType) {

        Method setterMethod = reflectionHelper.getMethodByNameAndTypes(beanClass, fieldName, fieldType);

        if(setterMethod == null) {

            String setterName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

            setterMethod = reflectionHelper.getMethodByNameAndTypes(beanClass, setterName, fieldType);
        }

        return setterMethod;
    }
}
