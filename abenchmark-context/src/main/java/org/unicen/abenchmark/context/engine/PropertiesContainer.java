package org.unicen.abenchmark.context.engine;

import java.util.*;

class PropertiesContainer {

    private final Map<String, String> placeholderProperties;

    PropertiesContainer(Map<String, String> placeholderProperties) {

        this.placeholderProperties = (placeholderProperties != null) ? placeholderProperties : new HashMap<String, String>();
    }

    public String getPropertyValue(String value) {

        String resultValue = value;

        if (value != null && value.startsWith("${") && value.endsWith("}")) {

            String propertyName = value.substring(2, value.length() - 1);
            String propertyValue = placeholderProperties.get(propertyName);

            if (propertyValue == null) {
                throw new IllegalStateException("PropertyName not found - " + propertyName);
            }

            resultValue = propertyValue;
        }

        return resultValue;
    }
}
