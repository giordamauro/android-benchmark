package org.unicen.abenchmark.context.engine;

import org.unicen.abenchmark.context.model.Bean;

import java.util.*;

public class BeanContext {

    private final BeanContainer beanContainer;
    private final BeanFactory beanFactory;

    private BeanContext(Builder builder) {

        this.beanContainer = new BeanContainer();

        PropertiesContainer propertiesContainer = new PropertiesContainer(builder.placeholderProperties);
        this.beanFactory = new BeanFactory(beanContainer, propertiesContainer, builder.classLoader);
    }

    public <T> T createBean(Bean bean) {

        @SuppressWarnings("unchecked")
        T result = (T) beanFactory.createBean(bean);

        return result;
    }

    public List<Object> createBeans(List<Bean> beans) {

        Objects.requireNonNull(beans, "Beans cannot be null");

        List<Object> result = new ArrayList<>();

        for (Bean bean : beans) {
            Object instance = beanFactory.createBean(bean);
            result.add(instance);
        }
        return result;
    }

    public Object getBeanById(String ref) {
        return beanContainer.getBeanById(ref);
    }

    public <T> T getBeanByClass(Class<T> beanType) {
        return beanContainer.getBeanByClass(beanType);
    }

    public static class Builder {

        private Map<String, String> placeholderProperties;
        private ClassLoader classLoader;

        public Builder setPlaceholderProperties(Map<String, String> placeholderProperties) {

            Objects.requireNonNull(placeholderProperties, "PlaceholderProperties cannot be null");
            this.placeholderProperties = placeholderProperties;

            return this;
        }

        public Builder setClassLoader(ClassLoader classLoader) {

            Objects.requireNonNull(classLoader, "classLoader cannot be null");
            this.classLoader = classLoader;

            return this;
        }

        public BeanContext build() {
            return new BeanContext(this);
        }
    }
}
