package org.unicen.abenchmark.context.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ToString
@RequiredArgsConstructor
public class Context {

    private final PropertyPlaceholder placeholders;

    @NotNull
    private final List<Bean> beans;

    Context() {

        this.placeholders = null;
        this.beans = null;
    }
}
