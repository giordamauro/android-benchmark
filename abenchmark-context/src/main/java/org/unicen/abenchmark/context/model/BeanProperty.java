package org.unicen.abenchmark.context.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class BeanProperty extends BeanDeclaration {

    private final String name;

    BeanProperty() {
        this.name = null;
    }
}
