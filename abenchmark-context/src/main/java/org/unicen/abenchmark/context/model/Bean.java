package org.unicen.abenchmark.context.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ToString
@RequiredArgsConstructor
public class Bean {

    private final String id;

    @NotNull
    private final String type;

    private final List<BeanProperty> properties;

    private final List<BeanDeclaration> constructorArgs;

    private final FactoryBean factoryBean;

    private final String factoryMethod;

    Bean() {

        this.id = null;
        this.type = null;
        this.properties = null;
        this.constructorArgs = null;
        this.factoryBean = null;
        this.factoryMethod = null;
    }
}
