package org.unicen.abenchmark.context.engine;

import org.unicen.abenchmark.context.model.Bean;
import org.unicen.abenchmark.context.model.BeanDeclaration;
import org.unicen.abenchmark.context.model.FactoryBean;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

class BeanFactory {

    private final BeanContainer beanContainer;
    private final ReflectionHelper reflectionHelper;
    private final BeanValueTypeHelper valueTypeHelper;

    BeanFactory(BeanContainer beanContainer, PropertiesContainer propertiesContainer, ClassLoader classLoader) {

        Objects.requireNonNull(beanContainer, "beanContainer cannot be null");

        this.beanContainer = beanContainer;
        this.reflectionHelper = new ReflectionHelper(classLoader);

        this.valueTypeHelper = new BeanValueTypeHelper(beanContainer, this, propertiesContainer, reflectionHelper);
    }

    public Object createBean(Bean bean) {

        Object beanInstance = this.createBeanInstance(bean);
        valueTypeHelper.addBeanProperties(beanInstance, bean.getProperties());

        beanContainer.registerBeanInstance(beanInstance, bean.getId());

        return beanInstance;
    }

    private Object createBeanInstance(Bean bean) {

        Objects.requireNonNull(bean, "Bean cannot be null");

        if (bean.getFactoryBean() != null && bean.getConstructorArgs() != null) {
            throw new IllegalStateException("Cannot instantiate bean having factoryBean and constructorArgs, only one must be present - " + bean);
        }

        Object beanInstance;

        Class<?> beanClass = reflectionHelper.getClassForName(bean.getType());

        if (bean.getConstructorArgs() != null) {

            List<BeanDeclaration> constructorArgs = bean.getConstructorArgs();
            List<ValueType> argList = valueTypeHelper.getByDeclarationList(constructorArgs);

            if(bean.getFactoryMethod() == null) {
                beanInstance = this.createInstanceByConstructorArgs(beanClass, argList);
            }
            else {
                beanInstance = this.createInstanceByFactoryMethod(beanClass, bean.getFactoryMethod(), argList);
            }
        } else if (bean.getFactoryBean() != null) {

            FactoryBean factoryBean = bean.getFactoryBean();
            beanInstance = this.createInstanceByFactoryBean(beanClass, factoryBean);

        } else {
            beanInstance = this.createInstanceByDefaultConstructor(beanClass);
        }

        return beanInstance;
    }

    private Object createInstanceByDefaultConstructor(Class<?> beanClass) {

        try {
            return beanClass.newInstance();

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private Object createInstanceByConstructorArgs(Class<?> beanClass, List<ValueType> argList) {

        List<Object> values = valueTypeHelper.getValueTypesValues(argList);
        Object[] constructorArgs = values.toArray();

        Constructor<?>[] constructors = beanClass.getDeclaredConstructors();
        Object instance = null;
        int i = 0;

        while (instance == null && i < constructors.length) {
            try {
                instance = constructors[i].newInstance(constructorArgs);
            } catch (Exception e) {
                i++;
            }
        }

        if (instance == null) {
            throw new IllegalStateException(String.format("Exception creating instance for class '%s' by constructors %s - Args: %s",
                    beanClass.getName(), Arrays.asList(constructors), values));
        }

        return instance;
    }

    private Object createInstanceByFactoryMethod(Class<?> beanClass, String factoryMethod, List<ValueType> argList) {

        List<Method> methods = reflectionHelper.getMethodsByName(beanClass, factoryMethod);
        if(methods.isEmpty()) {
            throw new IllegalStateException(String.format("Factory method not found for class '%s' - Name: %s", beanClass.getName(), factoryMethod));
        }

        List<Object> values = valueTypeHelper.getValueTypesValues(argList);
        Object instance = reflectionHelper.getResultByInvokeMethods(methods, null, values);

        if (instance == null) {
            throw new IllegalStateException(String.format("Exception invoking Factory Method for class '%s' by methods %s - Args: %s", beanClass.getName(), methods, values));
        }

        if(!beanClass.isInstance(instance)) {
            throw new IllegalStateException(String.format("Instance from Factory Method invalid - Expected class '%s' : got '%s'", beanClass, instance.getClass()));
        }

        return instance;
    }

    private Object createInstanceByFactoryBean(Class<?> beanClass, FactoryBean factoryBean) {

        Object factory;

        String ref = factoryBean.getRef();
        if(ref != null) {
            factory = beanContainer.getBeanById(ref);
        }
        else {
            Bean bean = factoryBean.getBean();
            if(bean == null) {
                throw new NullPointerException("bean cannot be null");
            }
            factory = this.createBean(bean);
        }

        Class<?> factoryClass = factory.getClass();
        String method = factoryBean.getMethod();

        List<Method> methods = reflectionHelper.getMethodsByName(factoryClass, method);
        if(methods.isEmpty()) {
            throw new IllegalStateException(String.format("FactoryBean method not found for class '%s' - Name: %s", factoryClass, method));
        }

        Object instance = reflectionHelper.getResultByInvokeMethods(methods, factory, null);

        if (instance == null) {
            throw new IllegalStateException(String.format("Exception invoking FactoryBean method for class '%s' by methods %s", factoryClass, methods));
        }

        if(!beanClass.isInstance(instance)) {
            throw new IllegalStateException(String.format("Instance from FactoryBean invalid - Expected class '%s' : got '%s'", beanClass, instance.getClass()));
        }

        return instance;
    }
}
