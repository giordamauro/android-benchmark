package org.unicen.abenchmark.context.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
public class PropertyPlaceholder {

    private final List<String> files;

    private final Map<String, String> properties;

    PropertyPlaceholder() {

        this.files = null;
        this.properties = null;
    }
}
