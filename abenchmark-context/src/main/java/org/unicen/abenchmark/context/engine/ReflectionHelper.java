package org.unicen.abenchmark.context.engine;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by mgiorda on 2/5/17.
 */
public class ReflectionHelper {

    private final ClassLoader classLoader;

    public ReflectionHelper(ClassLoader classLoader) {

        this.classLoader = (classLoader != null) ? classLoader : this.getClass().getClassLoader();
    }

    public Class<?> getClassForName(String classType) {

        Objects.requireNonNull(classType, "ClassType cannot be null");

        try {
            return Class.forName(classType, true, classLoader);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setFieldValue(Object target, String fieldName, Object value) {

        Objects.requireNonNull(target, "Target cannot be null");
        Objects.requireNonNull(fieldName, "FieldName cannot be null");

        Class<?> targetClass = target.getClass();
        try {
            Field propertyField = targetClass.getDeclaredField(fieldName);

            boolean accessible = propertyField.isAccessible();
            propertyField.setAccessible(true);
            propertyField.set(target, value);
            propertyField.setAccessible(accessible);

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public Object invokeMethod(Method method, Object target, Object... values) {

        Objects.requireNonNull(method, "method cannot be null");
        Objects.requireNonNull(target, "target cannot be null");

        try {
            return method.invoke(target, values);

        } catch (Exception e) {
            throw new IllegalStateException("Exception invoking method", e);
        }
    }


    public Class<?> getTypeClassForName(String type) {

        Class<?> valueType = String.class;

        if (type != null) {

            if ("int".equals(type)) {
                valueType = int.class;
            } else if ("long".equals(type)) {
                valueType = long.class;
            } else if ("boolean".equals(type)) {
                valueType = boolean.class;
            } else if ("double".equals(type)) {
                valueType = double.class;
            } else if ("float".equals(type)) {
                valueType = float.class;
            } else {
                valueType = this.getClassForName(type);
            }
        }

        return valueType;
    }

    public Object getResultByInvokeMethods(List<Method> methods, Object target, List<Object> values) {

        Object[] methodArgs = (values != null) ? values.toArray() : null;

        Object instance = null;
        int i = 0;

        while (instance == null && i < methods.size()) {
            try {
                if(methodArgs != null) {
                    instance = methods.get(i).invoke(target, methodArgs);
                }
                else {
                    instance = methods.get(i).invoke(target);
                }
            } catch (Exception e) {
                i++;
            }
        }

        return instance;
    }

    public Method getMethodByNameAndTypes(Class<?> beanClass, String methodName, Class<?>... fieldTypes) {

        Objects.requireNonNull(beanClass, "beanClass cannot be null");
        Objects.requireNonNull(methodName, "methodName cannot be null");
        Objects.requireNonNull(fieldTypes, "fieldTypes cannot be null");

        Method method = null;

        try {
            method = beanClass.getDeclaredMethod(methodName, fieldTypes);

        } catch (NoSuchMethodException e) {
        }

        return method;
    }

    public List<Method> getMethodsByName(Class<?> beanClass, String methodName) {

        Objects.requireNonNull(beanClass, "beanClass cannot be null");
        Objects.requireNonNull(methodName, "methodName cannot be null");

        List<Method> methodsByName = new ArrayList<>();
        Method[] methods = beanClass.getDeclaredMethods();

        for(Method method : methods) {
            if(method.getName().equals(methodName)) {
                methodsByName.add(method);
            }
        }

        return methodsByName;
    }
}
