package org.unicen.abenchmark.context.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class BeanDeclaration {

    private final String value;

    private final String type;

    private final String ref;

    private final Bean bean;

    private final List<BeanDeclaration> list;

    BeanDeclaration() {

        this.value = null;
        this.type = null;
        this.ref = null;
        this.bean = null;
        this.list = null;
    }
}
