package org.unicen.abenchmark.context.support;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Objects;

public class JarFileClassLoader extends URLClassLoader {

    private JarFileClassLoader(ClassLoader parentClassLoader) {

        super(new URL[] {}, parentClassLoader);
    }

    private JarFileClassLoader addFilePath(String path) {

        Objects.requireNonNull(path, "path cannot be null");

        String urlPath = "jar:file://" + path + "!/";

        try {
            this.addURL(new URL(urlPath));
        }
        catch (MalformedURLException e) {
            throw new IllegalArgumentException("Exception adding filePath " + path, e);
        }

        return this;
    }

    private JarFileClassLoader addFilePaths(List<String> paths) {

        Objects.requireNonNull(paths, "paths cannot be null");

        for (String path : paths) {
            this.addFilePath(path);
        }

        return this;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private ClassLoader parentClassLoader;
        private List<String> filePaths;

        public Builder parentClassLoader(ClassLoader classLoader) {

            Objects.requireNonNull(classLoader, "classLoader cannot be null");
            this.parentClassLoader = classLoader;

            return this;
        }

        public Builder filePaths(List<String> filePaths) {

            Objects.requireNonNull(filePaths, "filePaths cannot be null");
            this.filePaths = filePaths;

            return this;
        }

        public JarFileClassLoader build() {

            Objects.requireNonNull(parentClassLoader, "parentClassLoader cannot be null");
            Objects.requireNonNull(filePaths, "filePaths cannot be null");

            JarFileClassLoader loader = new JarFileClassLoader(parentClassLoader);
            loader.addFilePaths(filePaths);

            return loader;
        }
    }
}