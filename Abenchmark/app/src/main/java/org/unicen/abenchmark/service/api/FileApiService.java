package org.unicen.abenchmark.service.api;

import com.google.common.io.Files;

import org.unicen.abenchmark.api.model.file.FileFormat;
import org.unicen.abenchmark.api.model.file.JarFile;
import org.unicen.abenchmark.model.api.JarFileApiEndpoint;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Mauro Giorda on 24/11/2016.
 */
public class FileApiService {

    private static FileApiService INSTANCE;

    private final JarFileApiEndpoint apiEndpoint;
    private final File internalDownloadFolder;

    private FileApiService(ApiClient apiClient, File internalDownloadFolder) {

        Objects.requireNonNull(apiClient, "apiClient cannot be null");
        Objects.requireNonNull(internalDownloadFolder, "internalDownloadFolder cannot be null");

        this.apiEndpoint = apiClient.createApiService(JarFileApiEndpoint.class);
        this.internalDownloadFolder = internalDownloadFolder;
    }

    public JarFile getJarFile(String jarName) {

        Objects.requireNonNull(jarName, "jarName cannot be null");

        Call<JarFile> call = apiEndpoint.findByName(jarName);

        try {
            Response<JarFile> response = call.execute();

            return response.body();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public File downloadDex(String fileName) {

        Objects.requireNonNull(fileName, "fileName cannot be null");

        Call<ResponseBody> call = apiEndpoint.downloadFile(fileName, FileFormat.DEX);
        try {
            Response<ResponseBody> response = call.execute();

            File file = new File(internalDownloadFolder, fileName);
            file.createNewFile();
            Files.asByteSink(file).write(response.body().bytes());

            return file;

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
    public static FileApiService getInstance(ApiClient apiClient, File internalDownloadFolder) {

        if(INSTANCE == null) {
            INSTANCE = new FileApiService(apiClient, internalDownloadFolder);
        }

        return INSTANCE;
    }
}
