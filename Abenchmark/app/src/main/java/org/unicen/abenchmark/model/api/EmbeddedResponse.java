package org.unicen.abenchmark.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by mgiorda on 12/10/16.
 */

public class EmbeddedResponse<T> {

    @SerializedName("_embedded")
    private Map<String, List<T>> embedded;

//    @SerializedName("_links")
//    private Map<String, Map<String, String>> links;

    public Map<String, List<T>> getEmbedded() {
        return embedded;
    }

    public void setEmbedded(Map<String, List<T>> embedded) {
        this.embedded = embedded;
    }
//
//    public Map<String, Map<String, String>> getLinks() {
//        return links;
//    }
//
//    public void setLinks(Map<String, Map<String, String>> links) {
//        this.links = links;
//    }
}
