package org.unicen.abenchmark.service.api;


import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.model.api.EmbeddedResponse;
import org.unicen.abenchmark.model.api.TestApiEndpoint;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mauro on 25/11/16.
 */
public class TestApiService {

    private static TestApiService INSTANCE;

    private final TestApiEndpoint apiEndpoint;

    private TestApiService(ApiClient apiClient) {

        Objects.requireNonNull(apiClient, "apiClient cannot be null");

        this.apiEndpoint = apiClient.createApiService(TestApiEndpoint.class);
    }

    public List<Test> getDevicePendingTests(Device device) {

        Objects.requireNonNull(device, "device cannot be null");

        Call<EmbeddedResponse<Test>> call = apiEndpoint.findByDeviceUuidAndStatus(device.getDeviceUuid(), TestStatus.PENDING);

        try {
            Response<EmbeddedResponse<Test>> response = call.execute();
            Map<String, List<Test>> result = response.body().getEmbedded();

            return result.get("tests");

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setTestStatus(Test test, TestStatus status) {

        Objects.requireNonNull(test, "test cannot be null");
        Objects.requireNonNull(status, "status cannot be null");

        Call<ResponseBody> call = apiEndpoint.setTestStatus(test.getId(), status);

        try {
            call.execute();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static TestApiService getInstance(ApiClient apiClient) {

        if(INSTANCE == null) {
            INSTANCE = new TestApiService(apiClient);
        }

        return INSTANCE;
    }
}
