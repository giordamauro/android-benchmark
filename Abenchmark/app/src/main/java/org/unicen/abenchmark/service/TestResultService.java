package org.unicen.abenchmark.service;

import org.unicen.abenchmark.api.model.result.Metric;
import org.unicen.abenchmark.api.model.result.OperationTestResult;
import org.unicen.abenchmark.api.model.result.TestResult;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.core.operation.OperationResult;
import org.unicen.abenchmark.core.operation.OperationRun;
import org.unicen.abenchmark.core.test.TestRun;
import org.unicen.abenchmark.service.api.TestResultApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TestResultService {

    private static TestResultService INSTANCE;

    private final TestResultApiService testResultApiService;

    private TestResultService(TestResultApiService testResultApiService) {

        Objects.requireNonNull(testResultApiService, "testResultApiService cannot be null");

        this.testResultApiService = testResultApiService;
    }

    public void saveTestRuns(Test test, List<TestRun> testRuns) {

        Objects.requireNonNull(test, "test cannot be null");
        Objects.requireNonNull(testRuns, "testRuns cannot be null");

        List<OperationTestResult> operationTestResults = new ArrayList<>();
        for (TestRun testRun : testRuns) {

            OperationTestResult testResult = this.getOperationTestResult(testRun);
            operationTestResults.add(testResult);
        }

        TestResult testResult = new TestResult(test.getId(), operationTestResults);

        testResultApiService.saveTestResult(testResult);
    }

    public OperationTestResult getOperationTestResult(TestRun testRun) {

        Objects.requireNonNull(testRun, "testRun cannot be null");

        String operationType = testRun.getTestRunner()
                .getOperationRunner().getOperation()
                .getClass().getName();

        List<Metric> operationMetrics = new ArrayList<>();
        List<OperationRun> iterations = testRun.getIterations();

        for (OperationRun operationRun : iterations) {

            List<Metric> opMetrics = this.getResultMetrics(operationRun.getMetrics());
            operationMetrics.addAll(opMetrics);
        }

        Long testTime = this.getTestExecutionTime(testRun.getIterations());

        List<Metric> testMetrics = this.getResultMetrics(testRun.getMetrics());

        return OperationTestResult.builder()
                .operationType(operationType)
                .testTime(testTime)
                .operationMetrics(operationMetrics)
                .testMetrics(testMetrics)
                .build();
    }

    private List<Metric> getResultMetrics(Map<String, Object> metrics) {

        List<Metric> resultMetrics = new ArrayList<>();

        for (Map.Entry<String, Object> metric : metrics.entrySet()) {

            Object metricValue = metric.getValue();
            String valueType = (metricValue != null) ? metricValue.getClass().getName() : null;

            Metric resultMetric = new Metric(metric.getKey(), valueType, metricValue);
            resultMetrics.add(resultMetric);
        }

        return resultMetrics;
    }

    private long getTestExecutionTime(List<OperationRun> iterations) {

        OperationResult first = iterations.get(0).getResult().get();
        OperationResult last = iterations.get(iterations.size() - 1).getResult().get();

        return last.getEndTime() - first.getStartTime();
    }

    public static TestResultService getInstance(TestResultApiService testResultApiService) {

        if (INSTANCE == null) {
            INSTANCE = new TestResultService(testResultApiService);
        }

        return INSTANCE;
    }
}