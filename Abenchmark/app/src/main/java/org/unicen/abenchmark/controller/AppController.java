package org.unicen.abenchmark.controller;


import android.support.v7.app.AppCompatActivity;

import org.unicen.abenchmark.MainActivity;
import org.unicen.abenchmark.annotation.loader.TestAnnotationLoader;
import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.controller.fsm.FiniteStateMachine;
import org.unicen.abenchmark.controller.fsm.StateCallback;
import org.unicen.abenchmark.controller.fsm.StateContext;
import org.unicen.abenchmark.controller.job.InitController;
import org.unicen.abenchmark.core.test.TestRun;
import org.unicen.abenchmark.core.test.TestRunner;
import org.unicen.abenchmark.model.TestBenchmarks;
import org.unicen.abenchmark.service.DexFileService;
import org.unicen.abenchmark.service.TestResultService;
import org.unicen.abenchmark.service.TestRunnerService;
import org.unicen.abenchmark.service.api.ApiClient;
import org.unicen.abenchmark.service.api.DeviceApiService;
import org.unicen.abenchmark.service.api.FileApiService;
import org.unicen.abenchmark.service.api.TestApiService;
import org.unicen.abenchmark.service.api.TestResultApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by mauro on 25/11/16.
 */
public class AppController {

    private static final String CONFIG_PROPERTIES_FILE_NAME = "config.properties";

    private static final String ABENCHMARK_API_URL_PROPERTY = "abenchmark-api.url";
    private static final String POLLING_RETRY_SECONDS_PROPERTY = "polling.retry.seconds";
    private static final int DEFAULT_POLLING_RETRY_SECONDS = 3;

    private static AppController INSTANCE;

    private final MainActivity activity;
    private final FiniteStateMachine finiteStateMachine;

    private final TestApiService testApiService;

    private final TestRunnerService testRunnerService;
    private final TestResultService testResultService;

    private final Integer pollingRetrySeconds;

    private AppController(MainActivity activity) {

        Objects.requireNonNull(activity, "activity cannot be null");

        this.activity = activity;

        Properties configProperties = getAssetProperties(activity, CONFIG_PROPERTIES_FILE_NAME);
        String apiHostUrl = configProperties.getProperty(ABENCHMARK_API_URL_PROPERTY);

        if(apiHostUrl == null) {
            throw new IllegalStateException(String.format("Api host url is null for property %s in file %s", ABENCHMARK_API_URL_PROPERTY, CONFIG_PROPERTIES_FILE_NAME));
        }

        this.pollingRetrySeconds = Integer.valueOf(configProperties.getProperty(POLLING_RETRY_SECONDS_PROPERTY, String.valueOf(DEFAULT_POLLING_RETRY_SECONDS)));

        ApiClient apiClient = ApiClient.getInstance(apiHostUrl);
        this.testApiService = TestApiService.getInstance(apiClient);
        DeviceApiService deviceApiService = DeviceApiService.getInstance(apiClient);
        FileApiService fileApiService = FileApiService.getInstance(apiClient, activity.getFilesDir());

        DexFileService dexFileService = DexFileService.getInstance(fileApiService, activity.getFilesDir());
        TestAnnotationLoader testAnnotationLoader = TestAnnotationLoader.getInstance();
        this.testRunnerService = TestRunnerService.getInstance(dexFileService, testAnnotationLoader);

        TestResultApiService testResultApiService = TestResultApiService.getInstance(apiClient);
        this.testResultService = TestResultService.getInstance(testResultApiService);

        InitController initController = InitController.getInstance(activity, deviceApiService);

        this.finiteStateMachine = FiniteStateMachine.getInstance();
        finiteStateMachine.initializing(initController);
        this.setWaitingForTestsBehavior(finiteStateMachine);
        this.setPreparingTestsBehavior(finiteStateMachine);
        this.setRunningTestsBehavior(finiteStateMachine);
    }

    public void startApp() {
        finiteStateMachine.start();
    }

    private void setWaitingForTestsBehavior(FiniteStateMachine finiteStateMachine) {

        finiteStateMachine.waitingForTests(new StateCallback<Device>() {

            @Override
            public void call(Device input, StateContext context) {
                handleWaitingForTestPlanBehavior(input, context);
            }
        });
    }

    private void setPreparingTestsBehavior(FiniteStateMachine finiteStateMachine) {

        finiteStateMachine.preparingTests(new StateCallback<List<Test>>() {

            @Override
            public void call(List<Test> input, StateContext context) {
                handlePreparingTestsBehavior(input, context);
            }
        });
    }

    private void setRunningTestsBehavior(FiniteStateMachine finiteStateMachine) {

        finiteStateMachine.runningTests(new StateCallback<List<TestBenchmarks>>() {

            @Override
            public void call(List<TestBenchmarks> input, StateContext context) {
                handleRunningTestPlanBehavior(input, context);
            }
        });
    }


    private void handleWaitingForTestPlanBehavior(final Device device, final StateContext context) {

        activity.displayInDialog("WAITING FOR TESTS");

        final List<Test> tests = new ArrayList<>();

        new Thread(new Runnable() {
            @Override
            public void run() {

                do {
                    System.out.println("Fetching pending Tests for Device");

                    List<Test> testsResult = testApiService.getDevicePendingTests(device);
                    if (!testsResult.isEmpty()) {
                        tests.addAll(testsResult);
                    } else {
                        try {
                            Thread.sleep(pollingRetrySeconds * 1000);
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                }
                while (tests.isEmpty());

                context.setTests(tests);
            }
        }).start();
    }

    private void handlePreparingTestsBehavior(final List<Test> tests, final StateContext context) {

        activity.displayInDialog("PREPARING TESTS");

        new Thread(new Runnable() {

            @Override
            public void run() {

                List<TestBenchmarks> testList = new ArrayList<>();

                for(Test test : tests) {

                    try {
                        List<TestRunner> testRunners = testRunnerService.getTestRunnersFromTest(test);

                        TestBenchmarks testBenchmarks = new TestBenchmarks(test, testRunners);
                        testList.add(testBenchmarks);

                    } catch (Exception e) {
                        throw new IllegalStateException("Exception on context declaration", e);
                    }
                }
                if(testList.isEmpty()) {
                    throw new IllegalStateException("Exception preparing tests - Not any test found to run");
                }

                context.setTestRunners(testList);
            }
        }).start();
    }

    private void handleRunningTestPlanBehavior(final List<TestBenchmarks> tests, final StateContext context) {

        activity.displayInDialog("RUNNING TESTS");

        new Thread(new Runnable() {

            @Override
            public void run() {

                for (TestBenchmarks testBenchmarks : tests) {

                    Test test = testBenchmarks.getTest();
                    List<TestRunner> testRunners = testBenchmarks.getTestRunners();

                    testApiService.setTestStatus(test, TestStatus.RUNNING);

                    List<TestRun> testRuns = new ArrayList<>();
                    for (TestRunner testRunner : testRunners) {

                        TestRun testRun = testRunner.run();
                        testRuns.add(testRun);
                    }

                    testResultService.saveTestRuns(test, testRuns);
                }

                context.setTestsFinished();
            }
        }).start();
    }

    private static Properties getAssetProperties(AppCompatActivity activity, String fileName) {

        Objects.requireNonNull(activity, "activity cannot be null");
        Objects.requireNonNull(fileName, "fileName cannot be null");

        try{
            Properties properties = new Properties();
            properties.load(activity.getAssets().open(fileName));

            return properties;

        }catch(Exception e){
            throw new IllegalStateException(e);
        }
    }

    public static AppController getInstance(MainActivity activity) {

        if(INSTANCE == null) {
            INSTANCE = new AppController(activity);
        }

        return INSTANCE;
    }
}
