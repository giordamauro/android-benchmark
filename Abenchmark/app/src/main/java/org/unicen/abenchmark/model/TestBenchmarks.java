package org.unicen.abenchmark.model;

import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.core.test.TestRunner;

import java.util.List;

/**
 * Created by mgiorda on 2/4/17.
 */

public class TestBenchmarks {

    private final Test test;
    private final List<TestRunner> testRunners;

    public TestBenchmarks(Test test, List<TestRunner> testRunners) {
        this.test = test;
        this.testRunners = testRunners;
    }

    public Test getTest() {
        return test;
    }

    public List<TestRunner> getTestRunners() {
        return testRunners;
    }
}
