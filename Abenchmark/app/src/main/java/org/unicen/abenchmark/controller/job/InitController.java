package org.unicen.abenchmark.controller.job;

import org.unicen.abenchmark.MainActivity;
import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.device.DeviceInformation;
import org.unicen.abenchmark.controller.fsm.StateCallback;
import org.unicen.abenchmark.controller.fsm.StateContext;
import org.unicen.abenchmark.model.AndroidDevice;
import org.unicen.abenchmark.service.api.DeviceApiService;
import org.unicen.abenchmark.service.api.ServiceCallback;
import org.unicen.abenchmark.util.Optional;

import java.util.Objects;

/**
 * Created by mgiorda on 2/5/17.
 */

public class InitController implements StateCallback<Void> {

    private static InitController INSTANCE;

    private final MainActivity activity;
    private final DeviceApiService deviceApiService;

    public InitController(MainActivity activity, DeviceApiService deviceApiService) {

        Objects.requireNonNull(activity, "activity cannot be null");
        Objects.requireNonNull(deviceApiService, "deviceApiService cannot be null");

        this.activity = activity;
        this.deviceApiService = deviceApiService;
    }

    @Override
    public void call(Void input, final StateContext context) {

        activity.displayInDialog("INITIALIZING");

        final AndroidDevice androidDevice = AndroidDevice.getInstance();
        final String deviceUuid = androidDevice.getDeviceUuid();

        activity.displayLogLine("Searching for registered DeviceUUID: " + deviceUuid);

        deviceApiService.getDeviceByUuid(deviceUuid, new ServiceCallback<Optional<Device>>() {

            @Override
            public void onSuccess(Optional<Device> result) {
                handleGetDeviceByUuid(result, context, androidDevice);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throw new IllegalStateException(throwable);
            }
        });
    }

    private void handleGetDeviceByUuid(Optional<Device> result, final StateContext context, AndroidDevice androidDevice) {

        if (result.isPresent()) {
            context.setDevice(result.get());

        } else {

            activity.displayLogLine("Registering as new Device");

            DeviceInformation deviceInformation = androidDevice.getDeviceInformation();
            final Device device = new Device(androidDevice.getDeviceUuid(), deviceInformation);

            deviceApiService.createDevice(device, new ServiceCallback<Void>() {

                @Override
                public void onSuccess(Void result) {
                    context.setDevice(device);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throw new IllegalStateException(throwable);
                }
            });
        }
    }

    public static InitController getInstance(MainActivity activity, DeviceApiService deviceApiService) {

        if(INSTANCE == null) {
            INSTANCE = new InitController(activity, deviceApiService);
        }

        return INSTANCE;
    }
}
