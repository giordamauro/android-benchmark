package org.unicen.abenchmark;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.apache.log4j.BasicConfigurator;
import org.unicen.abenchmark.controller.AppController;

import java.util.Date;
import java.util.Objects;

import unicen.org.abenchmark.R;

public class MainActivity extends AppCompatActivity {

    private static final String APP_NAME = "ABENCHMARK";

    private ProgressDialog progressDialog;
    private AppController appController;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        BasicConfigurator.configure();

        if(appController == null) {
            this.appController = AppController.getInstance(this);
            appController.startApp();
        }
    }

    public void displayInDialog(String text) {

        Objects.requireNonNull(text, "text cannot be null");

        displayLogLine(text);

        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(MainActivity.this, "Application", text, true);
        } else {
            progressDialog.setMessage(text);
        }
    }

    public void displayLogLine(String logLine) {

        Objects.requireNonNull(logLine, "logLine cannot be null");

        TextView logText = (TextView) findViewById(R.id.logTextView);

        String newLog = String.format("%s\n- %s\n  %s", logText.getText(), new Date(), logLine);
        logText.setText(newLog);

        Log.i(APP_NAME, newLog);
    }
}
