package org.unicen.abenchmark.model.api;

import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TestApiEndpoint {

    @GET("/tests/search/findByDeviceUuidAndStatus")
    Call<EmbeddedResponse<Test>> findByDeviceUuidAndStatus(@Query("deviceUuid") String deviceUuid, @Query("status") TestStatus status);

    @PUT("/tests/{testId}/status")
    Call<ResponseBody> setTestStatus(@Path("testId") String testId, @Query("value") TestStatus status);
}