package org.unicen.abenchmark.service;

import org.unicen.abenchmark.annotation.loader.TestAnnotationLoader;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestConfig;
import org.unicen.abenchmark.context.engine.BeanContext;
import org.unicen.abenchmark.context.model.Bean;
import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.operation.OperationListener;
import org.unicen.abenchmark.core.test.RunnerEvaluator;
import org.unicen.abenchmark.core.test.TestListener;
import org.unicen.abenchmark.core.test.TestRunner;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dalvik.system.PathClassLoader;

/**
 * Created by Mauro Giorda on 24/11/2016.
 */
public class TestRunnerService {

    private static TestRunnerService INSTANCE;

    private final DexFileService dexFileService;
    private final TestAnnotationLoader testAnnotationLoader;

    private TestRunnerService(DexFileService dexFileService, TestAnnotationLoader testAnnotationLoader) {

        Objects.requireNonNull(dexFileService, "dexFileService cannot be null");
        Objects.requireNonNull(testAnnotationLoader, "testAnnotationLoader cannot be null");

        this.testAnnotationLoader = testAnnotationLoader;
        this.dexFileService = dexFileService;
    }

    public List<TestRunner> getTestRunnersFromTest(Test test) {

        Objects.requireNonNull(test, "test cannot be null");

        List<TestRunner> testRunners = new ArrayList<>();

        ClassLoader classLoader = this.getSourcesClassLoader(test.getSources());

        List<String> testClasses = test.getTestClasses();
        if(testClasses != null) {
            for (String testClass : testClasses) {

                @SuppressWarnings("unchecked")
                Class<Operation> benchmarkClass = (Class<Operation>) testAnnotationLoader.getAnnotationHelper().getClassForName(classLoader, testClass);
                TestRunner testRunner = testAnnotationLoader.getTestRunnerFromBenchmarkClass(benchmarkClass, classLoader);

                testRunners.add(testRunner);
            }
        }

        List<TestConfig> testConfigs = test.getTestConfigs();
        if(testConfigs != null) {
            for (TestConfig testConfig : testConfigs) {

                TestRunner testRunner = this.getTestRunnerFromTestConfig(testConfig, classLoader);

                testRunners.add(testRunner);
            }
        }

        return testRunners;
    }

    public TestRunner getTestRunnerFromTestConfig(TestConfig testConfig, ClassLoader classLoader) {

        Objects.requireNonNull(testConfig, "testConfig cannot be null");
        Objects.requireNonNull(classLoader, "classLoader cannot be null");

        TestRunnerBuilder runnerBuilder = TestRunner.builder();

        runnerBuilder.warmUpIterations(testConfig.getWarmUpIterations());

        BeanContext beanContext = new BeanContext.Builder()
                .setClassLoader(classLoader)
                .build();

        Operation operation = beanContext.createBean(testConfig.getOperation());
        runnerBuilder.operation(operation);

        RunnerEvaluator evaluator = beanContext.createBean(testConfig.getEvaluator());
        runnerBuilder.evaluator(evaluator);

        List<Bean> operationListenerBeans = testConfig.getOperationListeners();
        if (operationListenerBeans != null) {
            for (Bean operationListenerBean : operationListenerBeans) {

                OperationListener operationListener = beanContext.createBean(operationListenerBean);
                runnerBuilder.withOperationListener(operationListener);
            }
        }

        List<Bean> testListenerBeans = testConfig.getTestListeners();
        if (testListenerBeans != null) {
            for (Bean testListenerBean : testListenerBeans) {

                TestListener testListener = beanContext.createBean(testListenerBean);
                runnerBuilder.withTestListener(testListener);
            }
        }

        return runnerBuilder.build();
    }

    private ClassLoader getSourcesClassLoader(List<String> sources) {

        String dexPaths = (sources != null) ? this.getDexPathsFromIncludes(sources) : "";

        return new PathClassLoader(dexPaths, this.getClass().getClassLoader());
    }

    private String getDexPathsFromIncludes(List<String> includes) {

        Objects.requireNonNull(includes, "includes cannot be null");

        String filePaths = "";

        for(String include : includes) {
            File file = dexFileService.getFile(include);
            filePaths += file.getAbsolutePath() + ":";
        }

        return filePaths.isEmpty() ? "" : filePaths.substring(0, filePaths.length() - 1);
    }

    public static TestRunnerService getInstance(DexFileService dexFileService, TestAnnotationLoader testAnnotationLoader) {

        if(INSTANCE == null) {
            INSTANCE = new TestRunnerService(dexFileService, testAnnotationLoader);
        }

        return INSTANCE;
    }
}
