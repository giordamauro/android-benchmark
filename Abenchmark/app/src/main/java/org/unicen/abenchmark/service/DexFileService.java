package org.unicen.abenchmark.service;

import org.unicen.abenchmark.service.api.FileApiService;

import java.io.File;
import java.util.Objects;

/**
 * Created by Mauro Giorda on 24/11/2016.
 */
public class DexFileService {

    private static DexFileService INSTANCE;

    private final FileApiService fileApiService;
    private final File internalDownloadFolder;

    private DexFileService(FileApiService fileApiService, File internalDownloadFolder) {

        Objects.requireNonNull(fileApiService, "fileApiService cannot be null");
        Objects.requireNonNull(internalDownloadFolder, "internalDownloadFolder cannot be null");

        this.fileApiService = fileApiService;
        this.internalDownloadFolder = internalDownloadFolder;
    }

    public File getFile(String jarName) {

        Objects.requireNonNull(jarName, "jarName cannot be null");

        File file = new File(internalDownloadFolder, jarName);
        if(!file.exists()) {

            file = fileApiService.downloadDex(jarName);
        }

        return file;
    }

    public static DexFileService getInstance(FileApiService fileApiService, File internalDownloadFolder) {

        if(INSTANCE == null) {
            INSTANCE = new DexFileService(fileApiService, internalDownloadFolder);
        }

        return INSTANCE;
    }
}
