package org.unicen.abenchmark.controller.fsm;

/**
 * Created by mgiorda on 11/27/16.
 */
public interface StateCallback<C> {

    void call(C input, StateContext context);
}
