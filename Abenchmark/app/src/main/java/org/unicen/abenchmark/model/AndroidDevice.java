package org.unicen.abenchmark.model;

import android.os.Build;

import org.unicen.abenchmark.api.model.device.*;
import org.unicen.abenchmark.api.model.device.AndroidVersion;

import java.util.UUID;

/**
 * Created by Mauro Giorda on 24/11/2016.
 */
public class AndroidDevice {

    private static AndroidDevice INSTANCE;

    private String deviceUuid;
    private DeviceInformation deviceInformation;
    private AndroidVersion androidVersion;

    private AndroidDevice(){}

    public String getDeviceUuid() {

        if(deviceUuid == null) {
            DeviceInformation deviceInformation = getDeviceInformation();
            deviceUuid = calculateDeviceUuid(deviceInformation);
        }

        return deviceUuid;
    }

    public DeviceInformation getDeviceInformation() {

        if(deviceInformation == null) {
            AndroidVersion androidVersion = getAndroidVersion();
            deviceInformation = createDeviceInformation(androidVersion);
        }

        return deviceInformation;
    }

    public AndroidVersion getAndroidVersion() {

        if(androidVersion == null) {
            androidVersion = createAndroidVersion();
        }

        return androidVersion;
    }

    private static String calculateDeviceUuid(DeviceInformation deviceInfo) {

        String devIDShort = "35" + (deviceInfo.getBoard().length() % 10) + (deviceInfo.getBrand().length() % 10) + (deviceInfo.getDevice().length() % 10) + (deviceInfo.getManufacturer().length() % 10) + (deviceInfo.getModel().length() % 10) + (deviceInfo.getProduct().length() % 10);

        String serial = null;
        try {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();

            // Go ahead and return the serial for api => 9
            return "android-" + new UUID(devIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            // String needs to be initialized
            serial = "serial"; // some value
        }
        return "android-" + new UUID(devIDShort.hashCode(), serial.hashCode()).toString();
    }

    private static AndroidVersion createAndroidVersion() {

        return AndroidVersion.builder()
                .baseOs(Build.VERSION.BASE_OS)
                .codename(Build.VERSION.CODENAME)
                .incremental(Build.VERSION.INCREMENTAL)
                .release(Build.VERSION.RELEASE)
                .sdkInt(Build.VERSION.SDK_INT)
                .securityPatch(Build.VERSION.SECURITY_PATCH)
                .build();
    }

    private static DeviceInformation createDeviceInformation(AndroidVersion version) {

        return DeviceInformation.builder()
                .version(version)
                .board(Build.BOARD)
                .bootloader(Build.BOOTLOADER)
                .brand(Build.BRAND)
                .device(Build.DEVICE)
                .display(Build.DISPLAY)
                .fingerprint(Build.FINGERPRINT)
                .hardware(Build.HARDWARE)
                .host(Build.HOST)
                .id(Build.ID)
                .manufacturer(Build.MANUFACTURER)
                .model(Build.MODEL)
                .product(Build.PRODUCT)
                .serial(Build.SERIAL)
                .tags(Build.TAGS)
                .unknown(Build.UNKNOWN)
                .type(Build.TYPE)
                .user(Build.USER)
                .build();
    }

    public static AndroidDevice getInstance() {

        if(INSTANCE == null) {
            INSTANCE = new AndroidDevice();
        }

        return INSTANCE;
    }
}
