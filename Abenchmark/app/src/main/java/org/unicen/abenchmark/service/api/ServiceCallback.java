package org.unicen.abenchmark.service.api;

/**
 * Created by mgiorda on 11/27/16.
 */

public interface ServiceCallback<T> {

    void onSuccess(T result);

    void onFailure(Throwable throwable);
}
