package org.unicen.abenchmark.controller.fsm;

import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.model.TestBenchmarks;

import java.util.List;

import au.com.ds.ef.StatefulContext;

class FsmContext extends StatefulContext {

    private Device device;
    private List<Test> tests;
    private List<TestBenchmarks> testBenchmarkses;

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

    public void setTestBenchmarkses(List<TestBenchmarks> testBenchmarkses) {
        this.testBenchmarkses = testBenchmarkses;
    }

    public List<TestBenchmarks> getTestBenchmarkses() {
        return testBenchmarkses;
    }
}