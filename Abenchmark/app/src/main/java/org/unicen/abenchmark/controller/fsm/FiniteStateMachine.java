package org.unicen.abenchmark.controller.fsm;

import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.model.TestBenchmarks;

import java.util.List;
import java.util.Objects;

import au.com.ds.ef.EasyFlow;
import au.com.ds.ef.EventEnum;
import au.com.ds.ef.StateEnum;
import au.com.ds.ef.call.ContextHandler;

import static au.com.ds.ef.FlowBuilder.from;
import static au.com.ds.ef.FlowBuilder.on;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.INIT_FINISHED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.TESTS_PROVIDED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.TEST_RUNS_FINISHED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.TEST_RUNS_PROVIDED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.State.INITIALIZING;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.State.PREPARING_TESTS;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.State.RUNNING_TESTS;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.State.WAITING_FOR_TESTS;

/**
 * Created by Mauro Giorda on 24/11/2016.
 */
public class FiniteStateMachine {

    private static FiniteStateMachine INSTANCE;

    enum State implements StateEnum {
        INITIALIZING, WAITING_FOR_TESTS, PREPARING_TESTS, RUNNING_TESTS
    }

    enum Event implements EventEnum {
        INIT_FINISHED, TESTS_PROVIDED, TEST_RUNS_PROVIDED, TEST_RUNS_FINISHED
    }

    private final EasyFlow<FsmContext> flow;

    private FiniteStateMachine() {

        this.flow = from(INITIALIZING).transit(on(INIT_FINISHED)
                .to(WAITING_FOR_TESTS).transit(on(TESTS_PROVIDED)
                        .to(PREPARING_TESTS).transit(on(TEST_RUNS_PROVIDED)
                                .to(RUNNING_TESTS).transit(on(TEST_RUNS_FINISHED)
                                        .to(WAITING_FOR_TESTS)))))
                        .executor(new UiThreadExecutor());
    }

    public void initializing(final StateCallback<Void> callback) {

        Objects.requireNonNull(callback, "callback cannot be null");

        flow.whenEnter(INITIALIZING, new ContextHandler<FsmContext>() {
            @Override
            public void call(final FsmContext context) throws Exception {
                callback.call(null, new StateContext(context));
            }
        });
    }

    public void waitingForTests(final StateCallback<Device> callback) {

        Objects.requireNonNull(callback, "callback cannot be null");

        flow.whenEnter(WAITING_FOR_TESTS, new ContextHandler<FsmContext>() {
            @Override
            public void call(final FsmContext context) throws Exception {
                callback.call(context.getDevice(), new StateContext(context));
            }
        });
    }

    public void preparingTests(final StateCallback<List<Test>> callback) {

        Objects.requireNonNull(callback, "callback cannot be null");

        flow.whenEnter(PREPARING_TESTS, new ContextHandler<FsmContext>() {
            @Override
            public void call(final FsmContext context) throws Exception {
                callback.call(context.getTests(), new StateContext(context));
            }
        });
    }

    public void runningTests(final StateCallback<List<TestBenchmarks>> callback) {

        Objects.requireNonNull(callback, "callback cannot be null");

        flow.whenEnter(RUNNING_TESTS, new ContextHandler<FsmContext>() {
            @Override
            public void call(final FsmContext context) throws Exception {
                callback.call(context.getTestBenchmarkses(), new StateContext(context));
            }
        });
    }

    public void start() {
        flow.start(new FsmContext());
    }

    public static FiniteStateMachine getInstance() {

        if(INSTANCE == null) {
            INSTANCE = new FiniteStateMachine();
        }

        return INSTANCE;
    }
}
