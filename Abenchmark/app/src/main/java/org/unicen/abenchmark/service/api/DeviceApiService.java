package org.unicen.abenchmark.service.api;


import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.model.api.DeviceApiEndpoint;
import org.unicen.abenchmark.util.Optional;

import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mauro on 25/11/16.
 */

public class DeviceApiService {

    private static DeviceApiService INSTANCE;

    private final DeviceApiEndpoint apiEndpoint;

    private DeviceApiService(ApiClient apiClient) {

        Objects.requireNonNull(apiClient, "apiClient cannot be null");

        this.apiEndpoint = apiClient.createApiService(DeviceApiEndpoint.class);
    }

    public void getDeviceByUuid(final String deviceUuid, final ServiceCallback<Optional<Device>> callback) {

        Objects.requireNonNull(deviceUuid, "deviceUuid cannot be null");
        Objects.requireNonNull(callback, "callback cannot be null");

        Call<Device> call = apiEndpoint.getDeviceByUuid(deviceUuid);

        call.enqueue(new Callback<Device>() {

            @Override
            public void onResponse(Call<Device> call, Response<Device> response) {

                Optional<Device> result = null;

                if(response.code() == 200) {
                    result = Optional.of(response.body());
                }
                else if (response.code() == 404) {
                    result = Optional.empty();
                }

                if(result != null) {
                    callback.onSuccess(result);
                }
                else {
                    callback.onFailure(new IllegalStateException(response.toString()));
                }
            }

            @Override
            public void onFailure(Call<Device> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void createDevice(final Device device, final ServiceCallback<Void> callback) {

        Objects.requireNonNull(device, "device cannot be null");

        Call<ResponseBody> call = apiEndpoint.createDevice(device);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.code() == 201) {
                    callback.onSuccess(null);
                }
                else {
                    callback.onFailure(new IllegalStateException(response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public static DeviceApiService getInstance(ApiClient apiClient) {

        if(INSTANCE == null) {
            INSTANCE = new DeviceApiService(apiClient);
        }

        return INSTANCE;
    }
}
