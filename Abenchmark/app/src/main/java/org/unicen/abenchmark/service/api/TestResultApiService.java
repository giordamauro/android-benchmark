package org.unicen.abenchmark.service.api;

import org.unicen.abenchmark.api.model.result.TestResult;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.core.test.TestRun;
import org.unicen.abenchmark.model.api.EmbeddedResponse;
import org.unicen.abenchmark.model.api.TestResultApiEndpoint;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Mauro Giorda on 24/11/2016.
 */
public class TestResultApiService {

    private static TestResultApiService INSTANCE;

    private final TestResultApiEndpoint apiEndpoint;

    private TestResultApiService(ApiClient apiClient) {

        Objects.requireNonNull(apiClient, "apiClient cannot be null");

        this.apiEndpoint = apiClient.createApiService(TestResultApiEndpoint.class);
    }

    public void saveTestResult(TestResult testResult) {

        Objects.requireNonNull(testResult, "testResult cannot be null");

        Call<ResponseBody> call = apiEndpoint.saveTestResult(testResult);

        try {
            call.execute();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static TestResultApiService getInstance(ApiClient apiClient) {

        if(INSTANCE == null) {
            INSTANCE = new TestResultApiService(apiClient);
        }

        return INSTANCE;
    }
}
