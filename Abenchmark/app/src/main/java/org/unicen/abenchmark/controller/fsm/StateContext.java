package org.unicen.abenchmark.controller.fsm;

/**
 * Created by mgiorda on 11/27/16.
 */

import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.model.TestBenchmarks;

import java.util.List;
import java.util.Objects;

import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.INIT_FINISHED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.TESTS_PROVIDED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.TEST_RUNS_FINISHED;
import static org.unicen.abenchmark.controller.fsm.FiniteStateMachine.Event.TEST_RUNS_PROVIDED;

public class StateContext {

    private final FsmContext fsmContext;

    public StateContext(FsmContext fsmContext) {

        Objects.requireNonNull(fsmContext, "fsmContext cannot be null");
        this.fsmContext = fsmContext;
    }

    public void setDevice(Device device) {

        Objects.requireNonNull(device, "device cannot be null");

        fsmContext.setDevice(device);

        try {
            fsmContext.trigger(INIT_FINISHED);
        }
        catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public void setTests(List<Test> tests) {

        Objects.requireNonNull(tests, "tests cannot be null");

        fsmContext.setTests(tests);

        try {
            fsmContext.trigger(TESTS_PROVIDED);
        }
        catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public void setTestRunners(List<TestBenchmarks> tests) {

        Objects.requireNonNull(tests, "tests cannot be null");

        fsmContext.setTestBenchmarkses(tests);

        try {
            fsmContext.trigger(TEST_RUNS_PROVIDED);
        }
        catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public void setTestsFinished() {

        try {
            fsmContext.trigger(TEST_RUNS_FINISHED);
        }
        catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
