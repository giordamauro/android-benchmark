package org.unicen.abenchmark.model.api;

import org.unicen.abenchmark.api.model.file.FileFormat;
import org.unicen.abenchmark.api.model.file.JarFile;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JarFileApiEndpoint {

    @GET("/jarFiles/search/findByName")
    Call<JarFile> findByName(@Query("name") String name);

    @GET("/files/{fileName}")
    Call<ResponseBody> downloadFile(@Path("fileName") String fileName, @Query("format") FileFormat format);

}