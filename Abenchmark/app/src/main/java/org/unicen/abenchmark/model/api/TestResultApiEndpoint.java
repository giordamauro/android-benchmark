package org.unicen.abenchmark.model.api;

import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.result.TestResult;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TestResultApiEndpoint {

    @GET("/testResults/search/findByTestId")
    Call<TestResult> findByTestId(@Query("testId") String testId);

    @POST("/testResults")
    Call<ResponseBody> saveTestResult(@Body TestResult testResult);
}