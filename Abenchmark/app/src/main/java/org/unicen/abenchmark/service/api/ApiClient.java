package org.unicen.abenchmark.service.api;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Objects;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mauro on 25/11/16.
 */

public class ApiClient {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    private static ApiClient INSTANCE;

    private final Retrofit retrofit;

    private ApiClient(String hostUrl) {

        Objects.requireNonNull(hostUrl, "hostUrl cannot be null");

        Gson gson = new GsonBuilder()
                .setDateFormat(DATE_FORMAT)
                .create();

        this.retrofit = new Retrofit.Builder()
                .baseUrl(hostUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public <T> T createApiService(Class<T> endpoint) {
        return retrofit.create(endpoint);
    }

    public static ApiClient getInstance(String hostUrl) {

        if(INSTANCE == null) {
            INSTANCE = new ApiClient(hostUrl);
        }

        return INSTANCE;
    }
}
