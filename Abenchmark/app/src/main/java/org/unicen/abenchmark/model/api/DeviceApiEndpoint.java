package org.unicen.abenchmark.model.api;

import org.unicen.abenchmark.api.model.device.Device;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface DeviceApiEndpoint {

    @GET("/devices/search/findByDeviceUuid")
    Call<Device> getDeviceByUuid(@Query("deviceUuid") String deviceUuid);

    @POST("/devices")
    Call<ResponseBody> createDevice(@Body Device device);
}