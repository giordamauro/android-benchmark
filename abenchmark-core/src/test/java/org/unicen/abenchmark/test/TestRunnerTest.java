package org.unicen.abenchmark.test;

import org.junit.Test;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;
import org.unicen.abenchmark.core.test.TestRun;
import org.unicen.abenchmark.core.test.TestRunner;
import org.unicen.abenchmark.core.suite.SuiteRun;
import org.unicen.abenchmark.samples.knapsack.BacktrackingAlgorithm;
import org.unicen.abenchmark.samples.knapsack.GreedyAlgorithm;
import org.unicen.abenchmark.samples.knapsack.KnapsackProblem;
import org.unicen.abenchmark.support.AverageTimeMetricProvider;

import java.util.List;

import static org.unicen.abenchmark.fluent.TestOptionsFluentBuilder.options;
import static org.unicen.abenchmark.fluent.OperationFluentBuilder.operation;
import static org.unicen.abenchmark.fluent.SuiteFluentBuilder.testPlan;
import static org.unicen.abenchmark.fluent.SuiteOptionsBuilder.withOptions;
import static org.unicen.abenchmark.support.evaluators.Iterations.times;


/**
 * Created by Mauro on 09/11/2016.
 */
public class TestRunnerTest {

    @Test
    public void runTest() throws Exception {

        KnapsackProblem knapsackProblem = KnapsackProblem.generateRandomly(10, 20, 30, 0.8);

        System.out.println(knapsackProblem);

        BacktrackingAlgorithm backtrackingAlgorithm = new BacktrackingAlgorithm(knapsackProblem);
        GreedyAlgorithm greedyAlgorithm = new GreedyAlgorithm(knapsackProblem);

        TestRunnerBuilder runnerBuilder = TestRunner.builder()
                .evaluator(times(1))
                .withTestListener(new AverageTimeMetricProvider());

        TestRun result1 = runnerBuilder
                .operation(backtrackingAlgorithm)
                .build()
                .run();

        TestRun result2 = runnerBuilder
                .operation(greedyAlgorithm)
                .build()
                .run();

        System.out.println(result1.getIterations().get(0).getResult());
        System.out.println(result2.getIterations().get(0).getResult());
    }

    @Test
    public void runTestPlan() throws Exception {

        KnapsackProblem knapsackProblem = KnapsackProblem.generateRandomly(10, 20, 30, 0.8);
        System.out.println(knapsackProblem);

        SuiteRun suiteRun = testPlan()
                .ofTests(
                        withOptions(
                                options()
                                .withListener(new AverageTimeMetricProvider())
                        ).andEvaluator(times(1)
                        ).andOperation(
                                operation()
                                .withInput(knapsackProblem)
                                .andClass(BacktrackingAlgorithm.class)
                        )
                        .andOperation(operation()
                                .withInput(knapsackProblem)
                                .andClass(BacktrackingAlgorithm.class)
                        )
                ).run();

        List<TestRun> tests = suiteRun.getTests();

        System.out.println(tests.get(0).getIterations().get(0).getResult());
        System.out.println(tests.get(1).getIterations().get(0).getResult());
    }

}