package org.unicen.abenchmark.samples.knapsack;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class KnapsackProblem {

    private final List<KnapsackItem> items;
    private final int knapsackWeight;

    public KnapsackProblem(List<KnapsackItem> items, int knapsackWeight) {

        Objects.requireNonNull(items, "items cannot be null");

        this.items = items;
        this.knapsackWeight = knapsackWeight;
    }

    public List<KnapsackItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    public int getKnapsackWeight() {
        return knapsackWeight;
    }

    @Override
    public String toString() {
        return "KnapsackProblem{" +
                "items=" + items +
                ", knapsackWeight=" + knapsackWeight +
                '}';
    }

    public static KnapsackProblem generateRandomly(int numItems, int maxItemValue, int maxItemWeight, double knapsackWeightRatio) {

        if (numItems < 2) {
            throw new IllegalArgumentException("The number of items must be greater or equal than 2");
        }

        if (maxItemValue < 1 || maxItemWeight < 1) {
            throw new IllegalArgumentException("Max item value and weight must be greater or equal than 1");
        }

        if (knapsackWeightRatio <= 0.0 || knapsackWeightRatio >= 1) {
            throw new IllegalArgumentException("The Knapsack Weight Ratio must be in the range (0.0,1.0)");
        }

        List<KnapsackItem> items = new ArrayList<>();
        int totalWeight = 0;

        for (int i = 0; i < numItems; i++) {

            int itemValue = (int) (Math.random() * (maxItemValue - 1)) + 1;
            int itemWeight = (int) (Math.random() * (maxItemWeight - 1)) + 1;

            items.add(new KnapsackItem(itemValue, itemWeight));
            totalWeight += itemWeight;
        }

        int knapsackWeight = (int) (((double) totalWeight) * knapsackWeightRatio);

        return new KnapsackProblem(items, knapsackWeight);
    }
}
