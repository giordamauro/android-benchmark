package org.unicen.abenchmark.samples.knapsack;

import org.unicen.abenchmark.core.operation.Operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BacktrackingAlgorithm implements Operation<List<KnapsackItem>> {

    private final KnapsackProblem knapsackProblem;

    public BacktrackingAlgorithm(KnapsackProblem knapsackProblem) {

        Objects.requireNonNull(knapsackProblem, "knapsackProblem cannot be null");

        this.knapsackProblem = knapsackProblem;
    }

    @Override
    public List<KnapsackItem> execute() {

        return getBestSolution(0, new ArrayList<KnapsackItem>());
    }

    private List<KnapsackItem> getBestSolution(int index, List<KnapsackItem> currentSolution) {

        if(index == knapsackProblem.getItems().size()) {
            return currentSolution;
        }
        else {
            KnapsackItem item = knapsackProblem.getItems().get(index);
            List<KnapsackItem> notPickingSolution = getBestSolution(index + 1, currentSolution);

            if(sumOfWeights(currentSolution) + item.getWeight() > knapsackProblem.getKnapsackWeight()) {
                return notPickingSolution;
            }

            List<KnapsackItem> currentWithItem = new ArrayList<>(currentSolution);
            currentWithItem.add(item);

            List<KnapsackItem> pickingSolution = getBestSolution(index + 1, currentWithItem);

            return (sumOfValues(pickingSolution) >= sumOfValues(notPickingSolution)) ? pickingSolution : notPickingSolution;
        }
    }

    private static int sumOfValues(List<KnapsackItem> items) {

        int value = 0;

        for(KnapsackItem item : items) {
            value += item.getValue();
        }

        return value;
    }

    private static int sumOfWeights(List<KnapsackItem> items) {

        int value = 0;

        for(KnapsackItem item : items) {
            value += item.getWeight();
        }

        return value;
    }
}
