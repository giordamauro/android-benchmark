package org.unicen.abenchmark.samples.knapsack;


import java.util.Comparator;

public class KnapsackItem implements Comparable<KnapsackItem> {

    private final int value;
    private final int weight;

    public KnapsackItem(int value, int weight) {

        this.value = value;
        this.weight = weight;
    }

    public int getValue() {
        return value;
    }

    public int getWeight() {
        return weight;
    }

    public double getRelativeValue() {
        return (double) value / (double) weight;
    }

    @Override
    public int compareTo(KnapsackItem otherItem) {
        return Double.valueOf(this.getRelativeValue()).compareTo(otherItem.getRelativeValue());
    }

    @Override
    public String toString() {
        return "KnapsackItem{" +
                "value=" + value +
                ", weight=" + weight +
                '}';
    }
}
