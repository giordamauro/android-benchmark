package org.unicen.abenchmark.samples.knapsack;

import org.unicen.abenchmark.core.operation.Operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class GreedyAlgorithm implements Operation<List<KnapsackItem>> {

    private final KnapsackProblem knapsackProblem;

    public GreedyAlgorithm(KnapsackProblem knapsackProblem) {

        Objects.requireNonNull(knapsackProblem, "knapsackProblem cannot be null");

        this.knapsackProblem = knapsackProblem;
    }

    @Override
    public List<KnapsackItem> execute() {

        List<KnapsackItem> sortedItems = new ArrayList<>(knapsackProblem.getItems());
        Collections.sort(sortedItems);

        int availableWeight = knapsackProblem.getKnapsackWeight();

        List<KnapsackItem> solution = new ArrayList<>();
        for (int i = sortedItems.size() - 1; i >= 0; i--) {

            KnapsackItem item = sortedItems.get(i);
            int itemWeight = item.getWeight();

            if (availableWeight > itemWeight) {

                solution.add(item);
                availableWeight -= itemWeight;
            }
        }
        return solution;
    }
}
