package org.unicen.abenchmark.samples;

import org.unicen.abenchmark.core.operation.Operation;

public class MatrixMultiplication implements Operation<double[][]> {

	private final double[][] A;
	private final double[][] B;

	public MatrixMultiplication(double[][] A, double[][] B) {

		int aColumns = A[0].length;
		int bRows = B.length;

		if (aColumns != bRows) {
			throw new IllegalArgumentException("A:Rows: " + aColumns + " did not match B:Columns " + bRows + ".");
		}

		this.A = A;
		this.B = B;
	}

	@Override
	public double[][] execute() {

		int aRows = A.length;
		int aColumns = A[0].length;
		int bColumns = B[0].length;

		double[][] C = new double[aRows][bColumns];

		for (int i = 0; i < aRows; i++) { // aRow
			for (int j = 0; j < bColumns; j++) { // bColumn
				for (int k = 0; k < aColumns; k++) { // aColumn
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}

		return C;
	}
}