package org.unicen.abenchmark.annotation.loader;

import org.unicen.abenchmark.annotation.*;
import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.operation.OperationListener;
import org.unicen.abenchmark.core.test.RunnerEvaluator;
import org.unicen.abenchmark.core.test.TestListener;
import org.unicen.abenchmark.core.test.TestRunner;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;
import org.unicen.abenchmark.support.evaluators.OnlyOnce;
import org.unicen.abenchmark.util.Optional;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by mgiorda on 2/11/17.
 */
public class TestAnnotationLoader {

    private static final RunnerEvaluator DEFAULT_EVALUATOR = new OnlyOnce();

    private static TestAnnotationLoader INSTANCE;

    private final AnnotationHelper annotationHelper;
    private final JarClassFinder jarClassFinder;

    private TestAnnotationLoader() {

        this.annotationHelper = AnnotationHelper.getInstance();
        this.jarClassFinder = JarClassFinder.getInstance(annotationHelper);
    }

    public List<TestRunner> loadAnnotationTests(ClassLoader classLoader, List<Class<Operation>> benchmarkClasses) {

        List<TestRunner> testRunners = new ArrayList<>();

        for (Class<Operation> benchmarkClass : benchmarkClasses) {

            TestRunner testRunner = this.getTestRunnerFromBenchmarkClass(benchmarkClass, classLoader);
            testRunners.add(testRunner);
        }

        return testRunners;
    }

    public List<Class<Operation>> getBenchmarkClasses(ClassLoader classLoader, List<String> jarPaths) {

        Objects.requireNonNull(classLoader, "classLoader cannot be null");
        Objects.requireNonNull(jarPaths, "jarPaths cannot be null");

        List<Class<Operation>> benchmarkClasses = new ArrayList<>();

        for (String jarPath : jarPaths) {

            List<Class<Operation>> operationClasses = jarClassFinder.findClassesImplementing(classLoader, Operation.class, jarPath);
            for (Class<Operation> operationClass : operationClasses) {

                Optional<Benchmark> annotation = annotationHelper.getAnnotation(operationClass, Benchmark.class);

                if (annotation.isPresent() && !annotation.get().ignore()) {

                    benchmarkClasses.add(operationClass);
                }
            }
        }

        return benchmarkClasses;
    }

    public TestRunner getTestRunnerFromBenchmarkClass(Class<Operation> benchmarkClass, ClassLoader classLoader) {

        Objects.requireNonNull(benchmarkClass, "benchmarkClass cannot be null");
        Objects.requireNonNull(classLoader, "classLoader cannot be null");

        TestRunnerBuilder runnerBuilder = TestRunner.builder();

        Operation operation = annotationHelper.createInstance(benchmarkClass);
        runnerBuilder.operation(operation);

        Optional<Evaluator> evaluator = annotationHelper.getAnnotation(benchmarkClass, Evaluator.class);

        RunnerEvaluator runnerEvaluator = (evaluator.isPresent()) ?
                annotationHelper.createInstance(evaluator.get().value()) : DEFAULT_EVALUATOR;

        runnerBuilder.evaluator(runnerEvaluator);

        Optional<WarmUp> warmUp = annotationHelper.getAnnotation(benchmarkClass, WarmUp.class);
        if (warmUp.isPresent()) {
            runnerBuilder.warmUpIterations(warmUp.get().iterations());
        }

        Optional<TestListeners> testListeners = annotationHelper.getAnnotation(benchmarkClass, TestListeners.class);
        if (testListeners.isPresent()) {

            for (Class<? extends TestListener> listenerClass : testListeners.get().value()) {

                TestListener listener = annotationHelper.createInstance(listenerClass);
                runnerBuilder.withTestListener(listener);
            }
        }

        Optional<OperationListeners> operationListeners = annotationHelper.getAnnotation(benchmarkClass, OperationListeners.class);
        if (operationListeners.isPresent()) {

            for (Class<? extends OperationListener> listenerClass : operationListeners.get().value()) {

                OperationListener listener = annotationHelper.createInstance(listenerClass);
                runnerBuilder.withOperationListener(listener);
            }
        }

        return runnerBuilder.build();
    }

    public AnnotationHelper getAnnotationHelper() {
        return annotationHelper;
    }

    public JarClassFinder getJarClassFinder() {
        return jarClassFinder;
    }

    public static TestAnnotationLoader getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new TestAnnotationLoader();
        }

        return INSTANCE;
    }
}
