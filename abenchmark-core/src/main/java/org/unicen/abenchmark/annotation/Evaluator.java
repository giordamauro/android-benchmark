package org.unicen.abenchmark.annotation;

import org.unicen.abenchmark.core.test.RunnerEvaluator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Mauro on 16/11/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Evaluator {

    Class<? extends RunnerEvaluator> value();
}
