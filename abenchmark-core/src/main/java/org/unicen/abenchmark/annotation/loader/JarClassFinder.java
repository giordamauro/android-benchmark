package org.unicen.abenchmark.annotation.loader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by mgiorda on 2/11/17.
 */
public class JarClassFinder {

    private static final String CLASS_FILE_EXTENSION = ".class";

    private static JarClassFinder INSTANCE;

    private final AnnotationHelper annotationHelper;

    private JarClassFinder(AnnotationHelper annotationHelper) {

        Objects.requireNonNull(annotationHelper, "annotationHelper cannot be null");

        this.annotationHelper = annotationHelper;
    }

    public <T> List<Class<T>> findClassesImplementing(ClassLoader classLoader, Class<T> interfaceClass, String jarFullPath) {

        Objects.requireNonNull(interfaceClass, "interfaceClass cannot be null");

        if (!interfaceClass.isInterface()) {
            throw new IllegalArgumentException("Class must be an interface - " + interfaceClass.getName());
        }

        List<Class<?>> jarClasses = this.findClasses(classLoader, jarFullPath);

        List<Class<T>> implementors = new ArrayList<>();
        for (Class<?> jarClass : jarClasses) {

            if (interfaceClass.isAssignableFrom(jarClass)) {

                @SuppressWarnings("unchecked")
                Class<T> implementsClass = (Class<T>) jarClass;

                implementors.add(implementsClass);
            }
        }

        return implementors;
    }

    public List<Class<?>> findClasses(ClassLoader classLoader, String jarFullPath) {

        Objects.requireNonNull(classLoader, "classLoader cannot be null");
        Objects.requireNonNull(jarFullPath, "jarFullPath cannot be null");

        List<Class<?>> jarClasses = new ArrayList<>();

        try {
            JarFile jarFile = new JarFile(jarFullPath);
            Enumeration<JarEntry> entries = jarFile.entries();

            while (entries.hasMoreElements()) {
                JarEntry jarEntry = entries.nextElement();

                if (jarEntry.getName().endsWith(CLASS_FILE_EXTENSION)) {

                    String className = this.jarEntryToClassName(jarEntry.getName());
                    Class<?> loadedClass = annotationHelper.getClassForName(classLoader, className);

                    jarClasses.add(loadedClass);
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Exception finding classes for jar " + jarFullPath, e);
        }

        return jarClasses;
    }

    public String jarEntryToClassName(String jarEntryName) {

        Objects.requireNonNull(jarEntryName, "jarEntryName cannot be null");

        String classname = jarEntryName.replaceAll("/", "\\.");

        return classname.substring(0, classname.length() - CLASS_FILE_EXTENSION.length());
    }

    public static JarClassFinder getInstance(AnnotationHelper annotationHelper) {

        if(INSTANCE == null) {
            INSTANCE = new JarClassFinder(annotationHelper);
        }

        return INSTANCE;
    }
}
