package org.unicen.abenchmark.annotation.loader;

import org.unicen.abenchmark.util.Optional;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by mgiorda on 2/11/17.
 */
public class AnnotationHelper {

    private static AnnotationHelper INSTANCE;

    private AnnotationHelper() {

    }
    public Class<?> getClassForName(ClassLoader classLoader, String className) {

        Objects.requireNonNull(classLoader, "classLoader cannot be null");
        Objects.requireNonNull(classLoader, "classLoader cannot be null");

        try {
            return Class.forName(className, true, classLoader);

        } catch (Exception e) {
            throw new IllegalStateException(String.format("Class name '%s' not present in classloader", className), e);
        }
    }

    public <T> T createInstance(Class<T> aClass) {

        Objects.requireNonNull(aClass, "aClass cannot be null");

        try {
            return aClass.newInstance();

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public <A extends Annotation> Optional<A> getAnnotation(AnnotatedElement annotatedElement, Class<A> annotationType) {

        try {
            A annotation = annotatedElement.getAnnotation(annotationType);

            return Optional.ofNullable(annotation);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static AnnotationHelper getInstance() {

        if(INSTANCE == null) {
            INSTANCE = new AnnotationHelper();
        }

        return INSTANCE;
    }
}
