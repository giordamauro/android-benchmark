package org.unicen.abenchmark.core.operation;

public interface OperationFactory<T> {

	Operation<T> create();
}
