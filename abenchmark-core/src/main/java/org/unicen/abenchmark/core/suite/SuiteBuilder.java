package org.unicen.abenchmark.core.suite;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.test.TestRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro on 13/11/2016.
 */
public class SuiteBuilder {

    private final List<TestRunner> testRunners;
    private final List<SuiteListener> suiteListeners;

    public SuiteBuilder() {

        this.testRunners = new ArrayList<>();
        this.suiteListeners = new ArrayList<>();
    }

    public SuiteBuilder addListener(SuiteListener listener) {

        Objects.requireNonNull(listener, "listener cannot be null");

        suiteListeners.add(listener);
        return this;
    }

    public SuiteBuilder addTestRunner(TestRunner testRunner) {

        Objects.requireNonNull(testRunner, "testRunner cannot be null");

        testRunners.add(testRunner);
        return this;
    }
    
    public SuiteRunner build() {
        return new SuiteRunner(testRunners, suiteListeners);
    }
}
