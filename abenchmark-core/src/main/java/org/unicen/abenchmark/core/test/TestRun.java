package org.unicen.abenchmark.core.test;

import org.unicen.abenchmark.core.operation.OperationRun;

import java.util.*;

/**
 * Created by Mauro on 12/11/2016.
 */
public class TestRun {

    private final TestRunner testRunner;

    private final List<OperationRun> iterations;
    private final Map<String, Object> metrics;

    private boolean finished = false;

    TestRun(TestRunner testRunner) {

        Objects.requireNonNull(testRunner, "testRunner cannot be null");

        this.testRunner = testRunner;
        this.iterations = new ArrayList<>();
        this.metrics = new HashMap<>();
    }

    void addIteration(OperationRun operationRun) {

        Objects.requireNonNull(operationRun, "operationRun cannot be null");

        iterations.add(operationRun);
    }

    void setFinished() {
        this.finished = true;
    }

    public void registerMetric(String metricName, Object value) {

        Objects.requireNonNull(metricName, "metricName cannot be null");
        Objects.requireNonNull(value, "value cannot be null");

        metrics.put(metricName, value);
    }

    public boolean isFinished() {
        return finished;
    }

    public TestRunner getTestRunner() {
        return testRunner;
    }

    public List<OperationRun> getIterations() {
        return Collections.unmodifiableList(iterations);
    }

    public Map<String, Object> getMetrics() {
        return Collections.unmodifiableMap(metrics);
    }

    @Override
    public String toString() {
        return "TestRun{" +
                "testRunner=" + testRunner +
                ", iterations=" + iterations +
                ", metrics=" + metrics +
                '}';
    }
}
