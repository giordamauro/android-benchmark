package org.unicen.abenchmark.core.test;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.operation.OperationListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro on 09/11/2016.
 */
public class TestRunnerBuilder {

    private Operation operation;
    private RunnerEvaluator evaluator;

    private Integer warmupIterations;

    private List<OperationListener> operationListeners = new ArrayList<>();
    private List<TestListener> testListeners = new ArrayList<>();

    public TestRunnerBuilder operation(Operation operation) {

        Objects.requireNonNull(operation, "operation cannot be null");
        this.operation = operation;

        return this;
    }

    public TestRunnerBuilder evaluator(RunnerEvaluator evaluator) {

        Objects.requireNonNull(evaluator, "evaluator cannot be null");
        this.evaluator = evaluator;

        return this;
    }

    public TestRunnerBuilder warmUpIterations(Integer warmupIterations) {
        this.warmupIterations = warmupIterations;
        return this;
    }

    public TestRunnerBuilder withOperationListener(OperationListener operationListener) {

        Objects.requireNonNull(operationListener, "operationListener cannot be null");

        operationListeners.add(operationListener);
        return this;
    }

    public TestRunnerBuilder withTestListener(TestListener testListener) {

        Objects.requireNonNull(testListener, "testListener cannot be null");

        testListeners.add(testListener);
        return this;
    }

    public TestRunner build() {

        Objects.requireNonNull(operation, "operation cannot be null");
        Objects.requireNonNull(evaluator, "evaluator cannot be null");

        return new TestRunner(this);
    }

    Integer getWarmupIterations() {
        return warmupIterations;
    }

    List<OperationListener> getOperationListeners() {
        return operationListeners;
    }

    List<TestListener> getTestListeners() {
        return testListeners;
    }

    Operation getOperation() {
        return operation;
    }

    RunnerEvaluator getEvaluator() {
        return evaluator;
    }
}
