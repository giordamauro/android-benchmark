package org.unicen.abenchmark.core.operation;

public interface Operation<R> {

	R execute();
}
