package org.unicen.abenchmark.core.suite;

import org.unicen.abenchmark.core.test.TestRun;
import org.unicen.abenchmark.core.test.TestRunner;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro on 13/11/2016.
 */
public class SuiteRunner {

    private final List<TestRunner> testRunners;
    private final List<SuiteListener> suiteListeners;

    public SuiteRunner(List<TestRunner> testRunners, List<SuiteListener> suiteListeners) {

        Objects.requireNonNull(testRunners, "testRunners cannot be null");
        Objects.requireNonNull(suiteListeners, "suiteListeners cannot be null");

        if(testRunners.isEmpty()) {
            throw new IllegalArgumentException("TestRunners cannot be empty");
        }

        this.testRunners = testRunners;
        this.suiteListeners = suiteListeners;
    }

    public List<TestRunner> getTestRunners() {
        return Collections.unmodifiableList(testRunners);
    }

    public List<SuiteListener> getSuiteListeners() {
        return Collections.unmodifiableList(suiteListeners);
    }

    public SuiteRun run() {

        SuiteRun suiteRun = new SuiteRun(this);

        for (SuiteListener metricProvider : suiteListeners) {
            metricProvider.onTestPlanStart(suiteRun);
        }
        for (TestRunner testRunner : testRunners) {
            TestRun testRun = testRunner.run();
            suiteRun.addTest(testRun);

            for (SuiteListener metricProvider : suiteListeners) {
                metricProvider.onTestFinish(suiteRun, testRun);
            }
        }

        suiteRun.setFinished();
        for (SuiteListener metricProvider : suiteListeners) {
            metricProvider.onTestPlanFinish(suiteRun);
        }

        return suiteRun;
    }

    @Override
    public String toString() {
        return "SuiteRunner{" +
                "testRunners=" + testRunners +
                ", suiteListeners=" + suiteListeners +
                '}';
    }
}
