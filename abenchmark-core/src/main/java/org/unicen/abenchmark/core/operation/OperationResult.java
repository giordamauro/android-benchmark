package org.unicen.abenchmark.core.operation;

import java.util.Objects;

/**
 * Created by Mauro Giorda on 02/08/2016.
 * This class holds the Time Metric results for one operation execution.
 */
public class OperationResult<T> {

    private final T result;

    private final long startTime;
    private final long endTime;

    /**
     * Default Constructor. Enforces immutability
     *
     * @param result result of the execution
     * @param startTime long nanoseconds
     * @param endTime long nanoseconds
     */
    OperationResult(T result, Long startTime, Long endTime) {

        Objects.requireNonNull(startTime, "startTime cannot be null");
        Objects.requireNonNull(endTime, "endTime cannot be null");

        if(endTime < startTime) {
            throw new IllegalArgumentException("Times relation cannot be negative");
        }

        this.result = result;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long getExecutionTime() {
        return endTime - startTime;
    }

    public T getResult() {
        return result;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        return "OperationResult{" +
                "result=" + result +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
