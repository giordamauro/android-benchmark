package org.unicen.abenchmark.core.test;

/**
 * Created by Mauro on 09/11/2016.
 */
public interface RunnerEvaluator {

    boolean shouldTestContinue(TestRun testRun);
}
