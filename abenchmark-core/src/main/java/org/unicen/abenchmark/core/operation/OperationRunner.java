package org.unicen.abenchmark.core.operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro Giorda on 02/08/2016.
 *
 * Generates the time Metric for a given operation (in nanoseconds).
 */
public class OperationRunner {

    private final Operation operation;
    private final List<OperationListener> listeners;

    public OperationRunner(Operation operation, List<OperationListener> listeners) {

        Objects.requireNonNull(operation, "operation cannot be null");
        Objects.requireNonNull(listeners, "listeners cannot be null");

        this.operation = operation;
        this.listeners = new ArrayList<>(listeners);
    }

    public Operation getOperation() {
        return operation;
    }

    public List<OperationListener> getListeners() {
        return Collections.unmodifiableList(listeners);
    }

    public OperationRun runOperation() {

        OperationRun operationRun = new OperationRun(this);

        for (OperationListener metricProvider : listeners) {
            metricProvider.onOperationStart(operationRun);
        }

        OperationResult operationResult = executeOperation();
        operationRun.setResult(operationResult);

        for (OperationListener metricProvider : listeners) {
            metricProvider.onOperationFinish(operationRun);
        }

        return operationRun;
    }

    private OperationResult executeOperation() {

        long start = System.nanoTime();
        Object result = operation.execute();
        long end = System.nanoTime();

        return new OperationResult<>(result, start, end);
    }

    @Override
    public String toString() {
        return "OperationRunner{" +
                "operation=" + operation +
                ", listeners=" + listeners +
                '}';
    }
}
