package org.unicen.abenchmark.core.operation;

import org.unicen.abenchmark.util.Optional;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Mauro Giorda on 02/08/2016.
 * This class holds the Time Metric results for one operation execution.
 */
public class OperationRun {

    private final OperationRunner runner;
    private final Map<String, Object> metrics;

    private OperationResult result;

    OperationRun(OperationRunner runner) {

        Objects.requireNonNull(runner, "runner cannot be null");

        this.runner = runner;
        this.metrics = new HashMap<>();
    }

    void setResult(OperationResult result) {

        Objects.requireNonNull(result, "result cannot be null");

        this.result = result;
    }

    public void registerMetric(String metricName, Object value) {

        Objects.requireNonNull(metricName, "metricName cannot be null");
        Objects.requireNonNull(value, "value cannot be null");

        metrics.put(metricName, value);
    }

    public Optional<OperationResult> getResult() {
        return Optional.ofNullable(result);
    }

    public OperationRunner getRunner() {
        return runner;
    }

    public Map<String, Object> getMetrics() {
        return Collections.unmodifiableMap(metrics);
    }

    @Override
    public String toString() {
        return "OperationRun{" +
                "runner=" + runner +
                ", metrics=" + metrics +
                ", result=" + result +
                '}';
    }
}
