package org.unicen.abenchmark.core.suite;

import org.unicen.abenchmark.core.test.TestRun;

import java.util.*;

/**
 * Created by Mauro on 13/11/2016.
 */
public class SuiteRun {

    private final SuiteRunner suiteRunner;

    private final List<TestRun> tests;
    private final Map<String, Object> metrics;

    private boolean finished = false;

    SuiteRun(SuiteRunner suiteRunner) {

        Objects.requireNonNull(suiteRunner, "suiteRunner cannot be null");

        this.suiteRunner = suiteRunner;
        this.tests = new ArrayList<>();
        this.metrics = new HashMap<>();
    }

    void addTest(TestRun testRun) {

        Objects.requireNonNull(testRun, "testRun cannot be null");

        tests.add(testRun);
    }

    void setFinished() {
        this.finished = true;
    }

    public void registerMetric(String metricName, Object value) {

        Objects.requireNonNull(metricName, "metricName cannot be null");
        Objects.requireNonNull(value, "value cannot be null");

        metrics.put(metricName, value);
    }

    public boolean isFinished() {
        return finished;
    }

    public SuiteRunner getSuiteRunner() {
        return suiteRunner;
    }

    public List<TestRun> getTests() {
        return Collections.unmodifiableList(tests);
    }

    public Map<String, Object> getMetrics() {
        return Collections.unmodifiableMap(metrics);
    }

    @Override
    public String toString() {
        return "SuiteRun{" +
                "suiteRunner=" + suiteRunner +
                ", tests=" + tests +
                ", metrics=" + metrics +
                '}';
    }
}
