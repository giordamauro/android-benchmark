package org.unicen.abenchmark.core.operation;

/**
 * Created by Mauro on 09/11/2016.
 */
public interface OperationListener {

    void onOperationStart(OperationRun operationRun);

    void onOperationFinish(OperationRun operationRun);
}
