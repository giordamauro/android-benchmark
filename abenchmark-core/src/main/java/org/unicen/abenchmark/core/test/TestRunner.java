package org.unicen.abenchmark.core.test;

import org.unicen.abenchmark.core.operation.OperationRun;
import org.unicen.abenchmark.core.operation.OperationRunner;
import org.unicen.abenchmark.util.Optional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mauro on 09/11/2016.
 */
public class TestRunner {

    private final OperationRunner operationRunner;
    private final RunnerEvaluator evaluator;

    private final Integer warmUpIterations;
    private final List<TestListener> testListeners;

    TestRunner(TestRunnerBuilder builder) {

        this.evaluator = builder.getEvaluator();
        this.operationRunner = new OperationRunner(builder.getOperation(), builder.getOperationListeners());

        this.warmUpIterations = builder.getWarmupIterations();
        this.testListeners = new ArrayList<>(builder.getTestListeners());
    }

    public TestRun run() {

        TestRun testRun = new TestRun(this);

        for (TestListener metricProvider : testListeners) {
            metricProvider.onTestStart(testRun);
        }
        this.executeTestWarmUp();

        do {
            OperationRun testIteration = operationRunner.runOperation();
            testRun.addIteration(testIteration);

            for (TestListener metricProvider : testListeners) {
                metricProvider.onTestIteration(testRun, testIteration);
            }
        }
        while (evaluator.shouldTestContinue(testRun));

        testRun.setFinished();
        for (TestListener metricProvider : testListeners) {
            metricProvider.onTestFinish(testRun);
        }

        return testRun;
    }

    public OperationRunner getOperationRunner() {
        return operationRunner;
    }

    public RunnerEvaluator getEvaluator() {
        return evaluator;
    }

    public Optional<Integer> getWarmUpIterations() {
        return Optional.ofNullable(warmUpIterations);
    }

    public List<TestListener> getTestListeners() {
        return testListeners;
    }

    @Override
    public String toString() {
        return "TestRunner{" +
                "operationRunner=" + operationRunner +
                ", evaluator=" + evaluator +
                ", warmUpIterations=" + warmUpIterations +
                ", testListeners=" + testListeners +
                '}';
    }

    public static TestRunnerBuilder builder() {
        return new TestRunnerBuilder();
    }

    private void executeTestWarmUp() {

        Optional<Integer> warmUp = this.getWarmUpIterations();
        if (warmUp.isPresent()) {

            for (int i = 0; i < warmUp.get(); i++) {
                operationRunner.runOperation();
            }
        }
    }
}
