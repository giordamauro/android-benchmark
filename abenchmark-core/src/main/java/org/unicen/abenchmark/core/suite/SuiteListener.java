package org.unicen.abenchmark.core.suite;

import org.unicen.abenchmark.core.test.TestRun;

/**
 * Created by Mauro on 13/11/2016.
 */
public interface SuiteListener {

    void onTestPlanStart(SuiteRun suiteRun);

    void onTestFinish(SuiteRun suiteRun, TestRun test);

    void onTestPlanFinish(SuiteRun suiteRun);
}
