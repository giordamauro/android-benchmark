package org.unicen.abenchmark.core.test;

import org.unicen.abenchmark.core.operation.OperationRun;

/**
 * Created by Mauro on 09/11/2016.
 */
public interface TestListener {

    void onTestStart(TestRun testRun);

    void onTestIteration(TestRun testRun, OperationRun testIteration);

    void onTestFinish(TestRun testRun);
}
