package org.unicen.abenchmark.support.evaluators;

/**
 * Created by mgiorda on 2/5/17.
 */
public class OnlyOnce extends Iterations {

    public OnlyOnce() {
        super(1);
    }
}