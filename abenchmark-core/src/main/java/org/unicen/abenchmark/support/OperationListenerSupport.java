package org.unicen.abenchmark.support;

import org.unicen.abenchmark.core.operation.OperationListener;
import org.unicen.abenchmark.core.operation.OperationRun;

/**
 * Created by Mauro on 09/11/2016.
 */
public abstract class OperationListenerSupport implements OperationListener {

    @Override
    public void onOperationStart(OperationRun operationRun) {}

    @Override
    public void onOperationFinish(OperationRun operationRun) {}
}

