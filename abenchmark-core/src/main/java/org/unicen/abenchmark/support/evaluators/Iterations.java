package org.unicen.abenchmark.support.evaluators;

import org.unicen.abenchmark.core.test.RunnerEvaluator;
import org.unicen.abenchmark.core.test.TestRun;

import java.util.Objects;

/**
 * Created by mgiorda on 2/5/17.
 */
public class Iterations implements RunnerEvaluator {

    private final int iterations;

    public Iterations(Integer iterations) {

        Objects.requireNonNull(iterations, "iterations cannot be null");

        if(iterations <= 0) {
            throw new IllegalArgumentException("Iterations number must be positive");
        }

        this.iterations = iterations;
    }

    @Override
    public boolean shouldTestContinue(TestRun testRun) {

        return iterations > testRun.getIterations().size();
    }

    public static Iterations times(Integer iterations) {
        return new Iterations(iterations);
    }

    @Override
    public String toString() {
        return "Iterations{" +
                "iterations=" + iterations +
                '}';
    }
}