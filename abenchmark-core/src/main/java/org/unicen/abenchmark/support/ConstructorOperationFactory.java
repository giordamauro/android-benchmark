package org.unicen.abenchmark.support;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.operation.OperationFactory;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro on 15/11/2016.
 */
public class ConstructorOperationFactory<T> implements OperationFactory<T> {

    private Object[] inputs;
    private Class<? extends Operation> operationClass;

    public ConstructorOperationFactory(Class<? extends Operation> operationClass) {

        Objects.requireNonNull(operationClass, "operationClass cannot be null");

        this.operationClass = operationClass;
    }

    public ConstructorOperationFactory constructorArgs(Object ... inputs) {

        this.inputs = inputs;
        return this;
    }

    @Override
    public Operation<T> create() {

        try {
            if (inputs == null) {
                return operationClass.newInstance();
            }

            Constructor<?>[] constructors = operationClass.getDeclaredConstructors();
            if(constructors.length == 1) {

                @SuppressWarnings("unchecked")
                Constructor<? extends Operation> constructor = (Constructor<? extends Operation>) constructors[0];

                return constructor.newInstance(inputs);
            }

            List<Class<?>> inputClasses = new ArrayList<>();
            for(Object input : inputs) {
                inputClasses.add(input.getClass());
            }

            Class<?>[] inputClassesArray = inputClasses.toArray(new Class<?>[]{});
            Constructor<? extends Operation> constructor = operationClass.getDeclaredConstructor(inputClassesArray);

            return constructor.newInstance(inputs);
        }
        catch (Exception e) {

            List<Object> inputList = inputs != null ? Arrays.asList(inputs) : null;
            throw new IllegalStateException(String.format("Exception creating operation with class %s and inputs %s", operationClass, inputList), e);
        }
    }
}
