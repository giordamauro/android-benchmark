package org.unicen.abenchmark.support;

import org.unicen.abenchmark.core.operation.OperationResult;
import org.unicen.abenchmark.core.operation.OperationRun;
import org.unicen.abenchmark.core.test.TestRun;

import java.util.List;

/**
 * Created by Mauro on 09/11/2016.
 */
public class AverageTimeMetricProvider extends TestListenerSupport {

    public static final String AVG_TIME_METRIC = "avg-time";

    @Override
    public void onTestFinish(TestRun testRun) {

        List<OperationRun> iterations = testRun.getIterations();

        long timeSum = 0;
        for (OperationRun result : iterations) {

            OperationResult operationResult = result.getResult().get();
            timeSum += operationResult.getExecutionTime();
        }

        double avgTime = timeSum / (double) iterations.size();

        testRun.registerMetric(AVG_TIME_METRIC, avgTime);
    }
}

