package org.unicen.abenchmark.support;

import org.unicen.abenchmark.core.operation.OperationRun;
import org.unicen.abenchmark.core.test.TestListener;
import org.unicen.abenchmark.core.test.TestRun;

/**
 * Created by Mauro on 09/11/2016.
 */
public abstract class TestListenerSupport implements TestListener {

    @Override
    public void onTestStart(TestRun testRun) {}

    @Override
    public void onTestIteration(TestRun testRun, OperationRun testIteration) {}

    @Override
    public void onTestFinish(TestRun testRun) {}
}

