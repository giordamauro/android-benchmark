package org.unicen.abenchmark.fluent;

import org.unicen.abenchmark.core.operation.OperationListener;
import org.unicen.abenchmark.core.test.*;

/**
 * Created by Mauro on 09/11/2016.
 */
public class TestOptionsFluentBuilder {

    private TestRunnerBuilder builder;

    private TestOptionsFluentBuilder() {
        this.builder = new TestRunnerBuilder();
    }

    public static TestOptionsFluentBuilder options() {
        return new TestOptionsFluentBuilder();
    }

    public TestOptionsFluentBuilder warmUp(Integer warmupIterations) {
        builder.warmUpIterations(warmupIterations);
        return this;
    }

    public TestOptionsFluentBuilder withListener(OperationListener operationListener) {
        builder.withOperationListener(operationListener);
        return this;
    }

    public TestOptionsFluentBuilder withListener(TestListener testListener) {
        builder.withTestListener(testListener);
        return this;
    }

    TestRunnerBuilder getBuilder() {
        return builder;
    }
}
