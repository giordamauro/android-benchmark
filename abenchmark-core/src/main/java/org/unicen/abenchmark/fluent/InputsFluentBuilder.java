package org.unicen.abenchmark.fluent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mauro on 15/11/2016.
 */
public class InputsFluentBuilder {

    private List<Object[]> inputs;

    private InputsFluentBuilder() {
        this.inputs = new ArrayList<>();
    }

    public static InputsFluentBuilder inputs() {
        return new InputsFluentBuilder();
    }

    public And add(Object ... inputs) {

        this.inputs.add(inputs);

        return new And(this);
    }

    List<Object[]> getInputs() {
        return inputs;
    }

    public static class And extends InputsFluentBuilder {

        private InputsFluentBuilder builder;

        private And(InputsFluentBuilder builder) {
            this.builder = builder;
        }

        public And andAdd(Object ... inputs) {

            builder.add(inputs);

            return this;
        }
    }
}
