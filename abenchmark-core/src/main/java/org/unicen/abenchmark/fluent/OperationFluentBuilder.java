package org.unicen.abenchmark.fluent;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.support.ConstructorOperationFactory;

/**
 * Created by Mauro on 15/11/2016.
 */
public class OperationFluentBuilder {

    private OperationFluentBuilder() {
    }

    public static OperationFluentBuilder operation() {
        return new OperationFluentBuilder();
    }

    public WithInput withInput(Object ... inputs) {
        return new WithInput(inputs);
    }

    public Operation ofClass(Class<? extends Operation> operationClass) {

        return new ConstructorOperationFactory(operationClass)
                .create();
    }

    public static class WithInput {

        private final Object[] inputs;

        private WithInput(Object[] inputs) {
            this.inputs = inputs;
        }

        public Operation andClass(Class<? extends Operation> operationClass) {

            return new ConstructorOperationFactory(operationClass)
                    .constructorArgs(inputs)
                    .create();
        }
    }
}
