package org.unicen.abenchmark.fluent;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.suite.SuiteBuilder;
import org.unicen.abenchmark.core.suite.SuiteListener;
import org.unicen.abenchmark.core.suite.SuiteRunner;
import org.unicen.abenchmark.core.test.TestRunner;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;

import java.util.List;
import java.util.Objects;

import static org.unicen.abenchmark.fluent.OperationFluentBuilder.operation;
import static org.unicen.abenchmark.fluent.TestFluentBuilder.test;

/**
 * Created by Mauro on 13/11/2016.
 */
public class SuiteFluentBuilder {

    private final SuiteBuilder builder;

    private SuiteFluentBuilder() {

        this.builder = new SuiteBuilder();
    }

    public static SuiteFluentBuilder testPlan() {
        return new SuiteFluentBuilder();
    }

    public AndTests withListeners(SuiteListener... listeners) {

        Objects.requireNonNull(listeners, "listeners cannot be null");

        for(SuiteListener metricProvider : listeners) {
            builder.addListener(metricProvider);
        }
        return new AndTests(this);
    }

    public SuiteRunner ofTests(TestRunner ... testRunners) {

        Objects.requireNonNull(testRunners, "testRunners cannot be null");

        for(TestRunner testRunner : testRunners) {
            builder.addTestRunner(testRunner);
        }

        return builder.build();
    }

    public SuiteRunner ofTests(SuiteOptionsBuilder.AndOperation testBuilder) {

        Objects.requireNonNull(testBuilder, "testBuilder cannot be null");

        TestRunnerBuilder optionsBuilder = testBuilder.getOptions();
        List<Operation> operations = testBuilder.getOperations();

        for(Operation operation : operations) {

            TestRunner runner = optionsBuilder
                    .operation(operation)
                    .build();

            builder.addTestRunner(runner);
        }
        return builder.build();
    }

    public SuiteRunner ofTests(SuiteInputsBuilder.AndClass testBuilder) {

        Objects.requireNonNull(testBuilder, "testBuilder cannot be null");

        TestRunnerBuilder runnerBuilder = testBuilder.getOptions();
        List<Object[]> inputs = testBuilder.getInputs();
        Class<? extends Operation> operationClass = testBuilder.getOperationClass();

        for(Object[] input : inputs) {

            TestRunner testRunner = runnerBuilder
                    .operation(operation()
                            .withInput(input)
                            .andClass(operationClass))
                    .build();

            builder.addTestRunner(testRunner);
        }
        return builder.build();
    }

    public static class AndTests {

        private final SuiteFluentBuilder builder;

        private AndTests(SuiteFluentBuilder builder){
            this.builder = builder;
        }

        public SuiteRunner andTests(TestRunner ... testRunners) {
            return builder.ofTests(testRunners);
        }

        public SuiteRunner andTests(SuiteOptionsBuilder.AndOperation testBuilder) {
            return builder.ofTests(testBuilder);
        }

        public SuiteRunner andTests(SuiteInputsBuilder.AndClass testBuilder) {
            return builder.ofTests(testBuilder);
        }
    }
}
