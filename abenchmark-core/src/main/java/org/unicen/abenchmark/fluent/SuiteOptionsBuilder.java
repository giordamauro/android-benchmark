package org.unicen.abenchmark.fluent;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.test.RunnerEvaluator;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro on 13/11/2016.
 */
public class SuiteOptionsBuilder {

    private final TestRunnerBuilder options;

    private SuiteOptionsBuilder(TestRunnerBuilder options) {

        Objects.requireNonNull(options, "options cannot be null");

        this.options = options;
    }

    public static SuiteOptionsBuilder withOptions(TestOptionsFluentBuilder optionsBuilder) {

        Objects.requireNonNull(optionsBuilder, "optionsBuilder cannot be null");

        return new SuiteOptionsBuilder(optionsBuilder.getBuilder());
    }

    public AndEvaluator andEvaluator(RunnerEvaluator evaluator) {

        options.evaluator(evaluator);

        return new AndEvaluator(this);
    }

    public static class AndEvaluator {

        private final SuiteOptionsBuilder optionsBuilder;

        private AndEvaluator(SuiteOptionsBuilder suiteOptionsBuilder) {
            this.optionsBuilder = suiteOptionsBuilder;
        }

        public AndOperation andOperation(Operation operation) {
            return new AndOperation(this, operation);
        }

    }

    public static class AndOperation {

        private final AndEvaluator andEvaluator;
        private final List<Operation> operations = new ArrayList<>();

        private AndOperation(AndEvaluator andEvaluator, Operation operation) {

            this.andEvaluator = andEvaluator;
            this.andOperation(operation);
        }

        public AndOperation andOperation(Operation operation) {

            Objects.requireNonNull(operation, "operation cannot be null");
            operations.add(operation);

            return this;
        }

        TestRunnerBuilder getOptions() {
            return andEvaluator.optionsBuilder.options;
        }

        List<Operation> getOperations() {
            return operations;
        }
    }
}
