package org.unicen.abenchmark.fluent;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.test.RunnerEvaluator;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;

import java.util.List;
import java.util.Objects;

/**
 * Created by Mauro on 13/11/2016.
 */
public class SuiteInputsBuilder {

    private final List<Object[]> inputs;

    private SuiteInputsBuilder(List<Object[]> inputs) {

        Objects.requireNonNull(inputs, "inputs cannot be null");
        this.inputs = inputs;
    }

    public static SuiteInputsBuilder withInputs(List<Object[]> inputs) {
        return new SuiteInputsBuilder(inputs);
    }

    public static SuiteInputsBuilder withInputs(InputsFluentBuilder inputsBuilder) {
        return new SuiteInputsBuilder(inputsBuilder.getInputs());
    }

    public AndOptions andOptions(TestOptionsFluentBuilder optionsBuilder) {
        return new AndOptions(this, optionsBuilder.getBuilder());
    }

    public static class AndOptions {

        private final SuiteInputsBuilder inputsBuilder;
        private final TestRunnerBuilder options;

        private AndOptions(SuiteInputsBuilder builder, TestRunnerBuilder options) {

            Objects.requireNonNull(options, "options cannot be null");

            this.inputsBuilder = builder;
            this.options = options;
        }

        public AndEvaluator andEvaluator(RunnerEvaluator evaluator) {

            options.evaluator(evaluator);

            return new AndEvaluator(this);
        }
    }

    public static class AndEvaluator {

        private final AndOptions andOptions;

        private AndEvaluator(AndOptions andOptions) {
            this.andOptions = andOptions;
        }

        public AndClass andClass(Class<? extends Operation> operationClass) {
            return new AndClass(this, operationClass);
        }
    }

    public static class AndClass {

        private final AndEvaluator optionsBuilder;
        private final Class<? extends Operation> operationClass;

        private AndClass(AndEvaluator builder, Class<? extends Operation> operationClass) {

            Objects.requireNonNull(operationClass, "operationClass cannot be null");

            this.optionsBuilder = builder;
            this.operationClass = operationClass;
        }

        List<Object[]> getInputs() {
            return optionsBuilder.andOptions.inputsBuilder.inputs;
        }

        TestRunnerBuilder getOptions() {
            return optionsBuilder.andOptions.options;
        }

         Class<? extends Operation> getOperationClass() {
            return operationClass;
        }
    }
}
