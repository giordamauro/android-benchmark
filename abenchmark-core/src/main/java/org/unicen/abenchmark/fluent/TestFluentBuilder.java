package org.unicen.abenchmark.fluent;

import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.test.*;

import java.util.Objects;

/**
 * Created by Mauro on 09/11/2016.
 */
public class TestFluentBuilder {

    private TestFluentBuilder() {
    }

    public static TestFluentBuilder test() {
        return new TestFluentBuilder();
    }

    public OfOperation ofOperation(Operation operation) {
        return new OfOperation(operation);
    }

    public static class OfOperation {

        private final Operation operation;

        private OfOperation(Operation operation) {

            Objects.requireNonNull(operation, "operation cannot be null");

            this.operation = operation;
        }

        public AndEvaluator andEvaluator(RunnerEvaluator evaluator) {
            return new AndEvaluator(this, evaluator);
        }
    }

    public static class AndEvaluator {

        private final OfOperation ofOperation;
        private final RunnerEvaluator evaluator;

        private AndEvaluator(OfOperation ofOperation, RunnerEvaluator evaluator) {

            Objects.requireNonNull(evaluator, "evaluator cannot be null");

            this.ofOperation = ofOperation;
            this.evaluator = evaluator;
        }

        public TestRunner create() {
            return TestRunner.builder()
                    .operation(ofOperation.operation)
                    .evaluator(evaluator)
                    .build();
        }

        public TestRunner withOptions(TestOptionsFluentBuilder options) {

            Objects.requireNonNull(options, "options cannot be null");

            TestRunnerBuilder builder = options.getBuilder();
            return builder
                    .operation(ofOperation.operation)
                    .evaluator(evaluator)
                    .build();
        }
    }
}
