package org.unicen.abenchmark.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.api.repository.DeviceRepository;
import org.unicen.abenchmark.api.repository.TestRepository;

import java.util.Objects;

@RestController
public class TestController {

	private final TestRepository testRepository;

	@Autowired
	public TestController(TestRepository testRepository) {

		Objects.requireNonNull(testRepository, "testRepository cannot be null");

		this.testRepository = testRepository;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/tests/{testId}/status")
	public void updateTestSatus(@PathVariable String testId, @RequestParam TestStatus value) {

        Test test = testRepository.findOne(testId);

        if(test == null) {
            throw new ResourceNotFoundException(testId);
        }

        if ((test.getStatus() == TestStatus.PENDING && value == TestStatus.RUNNING) ||
                (test.getStatus() == TestStatus.RUNNING && value == TestStatus.DONE)) {
            test.setStatus(value);
        }
        else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, String.format("Cannot change from test state %s to %s", test.getStatus(), value));
        }

        testRepository.save(test);
	}
}
