package org.unicen.abenchmark.api.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
public class AwsConfiguration {

    @Value("${cloud.aws.bucket.name}")
    private String bucketName;

    @Value("${cloud.aws.credentials.accessKey}")
    private String awsKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String awsSecret;
}
