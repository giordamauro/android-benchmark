package org.unicen.abenchmark.api.service.upload;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.unicen.abenchmark.api.config.AwsConfiguration;
import org.unicen.abenchmark.api.service.UploaderService;

import java.io.InputStream;
import java.util.Objects;

@Component
@ConditionalOnProperty(prefix = "uploader.service", name = "type", havingValue = "s3")
public class S3Uploader implements UploaderService {

    private final String awsBucket;
    private final AmazonS3Client amazonS3Client;

    @Autowired
    public S3Uploader(AwsConfiguration awsConfiguration) {

        Objects.requireNonNull(awsConfiguration, "awsConfiguration cannot be null");
        this.awsBucket = awsConfiguration.getBucketName();

        AWSCredentials awsCredentials = new BasicAWSCredentials(awsConfiguration.getAwsKey(), awsConfiguration.getAwsSecret());
        this.amazonS3Client = new AmazonS3Client(awsCredentials);
    }

    @Override
    public String upload(MultipartFile file) {

        Objects.requireNonNull(file, "file cannot be null");

        String fileNameInS3 = file.getOriginalFilename();

        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(file.getContentType());
            metadata.setContentLength((long) file.getBytes().length);

            amazonS3Client.putObject(
                    new PutObjectRequest(awsBucket, fileNameInS3, file.getInputStream(), metadata)
                            .withCannedAcl(CannedAccessControlList.PublicRead));

            return "s3://" + awsBucket + "/" + fileNameInS3;

        } catch (Exception e) {
            throw new IllegalStateException("Exception uploading file to S3 - Name: "  + fileNameInS3, e);
        }
    }

    @Override
    public InputStream getFileContent(String id) {

        Objects.requireNonNull(id, "id cannot be null");

        S3Object s3Object = amazonS3Client.getObject(new GetObjectRequest(awsBucket, id));

        return s3Object.getObjectContent();
    }
}
