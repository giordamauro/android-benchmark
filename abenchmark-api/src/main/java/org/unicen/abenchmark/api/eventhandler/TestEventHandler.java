package org.unicen.abenchmark.api.eventhandler;

import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.unicen.abenchmark.annotation.loader.TestAnnotationLoader;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.api.service.TestContextService;
import org.unicen.abenchmark.core.operation.Operation;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@RepositoryEventHandler(Test.class)
public class TestEventHandler {

    private final TestContextService testContextService;
    private final TestAnnotationLoader testAnnotationLoader;

    public TestEventHandler(TestContextService testContextService) {

        Objects.requireNonNull(testContextService, "testContextService cannot be null");

        this.testContextService = testContextService;
        this.testAnnotationLoader = TestAnnotationLoader.getInstance();
    }

    @HandleBeforeCreate
    public void handleBeforeCreate(Test test) {

        test.setStatus(TestStatus.PENDING);

        List<String> sources = test.getSources();
        List<String> testClasses = (sources != null) ? this.getTestClasses(sources) : Collections.emptyList();

        test.setTestClasses(testClasses);
    }

    private List<String> getTestClasses(List<String> sources) {

        List<String> jarUrls = testContextService.getSourcePathUrls(sources);
        ClassLoader classLoader = testContextService.getJarUrlClassLoader(jarUrls);

        List<Class<Operation>> benchmarkClasses = testAnnotationLoader.getBenchmarkClasses(classLoader, jarUrls);

        return benchmarkClasses.stream()
                .map(Class::getName)
                .collect(Collectors.toList());
    }
}