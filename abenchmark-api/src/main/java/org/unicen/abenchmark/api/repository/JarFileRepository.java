package org.unicen.abenchmark.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.unicen.abenchmark.api.model.file.JarFile;

import java.util.Optional;

public interface JarFileRepository extends PagingAndSortingRepository<JarFile, String> {

    Optional<JarFile> findByName(@Param("name") String name);
}