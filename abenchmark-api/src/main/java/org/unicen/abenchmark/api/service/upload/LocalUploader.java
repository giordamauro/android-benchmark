package org.unicen.abenchmark.api.service.upload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.unicen.abenchmark.api.service.UploaderService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Component
@ConditionalOnProperty(prefix = "uploader.service", name = "type", havingValue = "local")
public class LocalUploader implements UploaderService {

    private static final Logger log = LoggerFactory.getLogger(LocalUploader.class);

    @Value("${files.directory}")
    private String filesDirectory;

    @Override
    public String upload(MultipartFile file) {

        Objects.requireNonNull(file, "file cannot be null");

        log.info("Uploading file to filesystem");

        try {
            String fileName = file.getOriginalFilename();
            Path path = Paths.get(filesDirectory, fileName);

            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            log.info("File successfully copied to local path {}", path.toString());

            return fileName;

        } catch (IOException e) {

            String message = "Unable to copy the specific file to the file system.";

            log.error(message, e);
            throw new IllegalStateException(message, e);
        }
    }

    @Override
    public InputStream getFileContent(String fileName) {

        Objects.requireNonNull(fileName, "fileName cannot be null");

        try {
            return new FileInputStream(filesDirectory + fileName);

        } catch (FileNotFoundException e) {

            throw new IllegalStateException("Exception getting file content for Id: " + fileName, e);
        }
    }
}
