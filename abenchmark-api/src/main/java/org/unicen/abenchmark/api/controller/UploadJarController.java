package org.unicen.abenchmark.api.controller;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.unicen.abenchmark.api.model.file.FileFormat;
import org.unicen.abenchmark.api.model.file.JarFile;
import org.unicen.abenchmark.api.service.DexService;
import org.unicen.abenchmark.api.service.JarFileService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

@RestController
@RequestMapping("/files")
public class UploadJarController {

	private final JarFileService jarFileService;

	@Autowired
	public UploadJarController(JarFileService jarFileService) {

        Objects.requireNonNull(jarFileService, "jarFileService cannot be null");

        this.jarFileService = jarFileService;
	}

	@RequestMapping(method = RequestMethod.POST)
	public JarFile handleFileUpload(@RequestParam MultipartFile file) {

		if (file.isEmpty()) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "File cannot be empty");
        }

        String jarName = file.getOriginalFilename().toLowerCase();
        if(!jarName.endsWith(".jar")) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "File can only be .jar extension");
        }

        String fileName = jarName.substring(0, jarName.length() - 4);

        jarFileService.getFileByName(fileName)
                .ifPresent(jarFile -> {
                    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, String.format("File named %s already exists", fileName));
                });

        return jarFileService.upload(file);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{fileName}")
    public void getFileContent(@PathVariable String fileName, @RequestParam FileFormat format, HttpServletResponse response) {

        JarFile jarFile = jarFileService.getFileByName(fileName.toLowerCase())
                .orElseThrow(ResourceNotFoundException::new);

        InputStream is = jarFileService.getFileContentByFormat(jarFile, format);

        try {
            IOUtils.copy(is, response.getOutputStream());

            response.flushBuffer();

        } catch (IOException e) {
            throw new IllegalStateException(String.format("Exception getting file content for fileName %s and format %s", fileName, format), e);
        }
    }
}