package org.unicen.abenchmark.api.eventhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.unicen.abenchmark.api.model.result.TestResult;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.api.repository.TestRepository;

import java.util.Objects;

@Component
@RepositoryEventHandler(TestResult.class)
public class TestResultEventHandler {

    private final TestRepository testRepository;

    @Autowired
    public TestResultEventHandler(TestRepository testRepository) {

        Objects.requireNonNull(testRepository, "testRepository cannot be null");

        this.testRepository = testRepository;
    }

    @HandleAfterCreate
    public void handleAfterCreate(TestResult testResult) {

        String testId = testResult.getTestId();
        Test test = testRepository.findOne(testId);

        test.setStatus(TestStatus.DONE);
        testRepository.save(test);
    }
}