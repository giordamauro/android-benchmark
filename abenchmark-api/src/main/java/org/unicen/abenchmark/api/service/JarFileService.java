package org.unicen.abenchmark.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.unicen.abenchmark.api.model.file.FileFormat;
import org.unicen.abenchmark.api.model.file.JarFile;
import org.unicen.abenchmark.api.repository.JarFileRepository;

import java.io.File;
import java.io.InputStream;
import java.util.Objects;
import java.util.Optional;

@Service
public class JarFileService {

	private final UploaderService uploaderService;
	private final JarFileRepository jarFileRepository;
    private final DexService dexService;

    @Value("${files.directory}")
    private String filesDirectory;

	@Autowired
	public JarFileService(UploaderService uploaderService, JarFileRepository jarFileRepository, DexService dexService) {

        Objects.requireNonNull(uploaderService, "uploaderService cannot be null");
        Objects.requireNonNull(jarFileRepository, "jarFileRepository cannot be null");
        Objects.requireNonNull(dexService, "dexService cannot be null");

        this.uploaderService = uploaderService;
		this.jarFileRepository = jarFileRepository;
        this.dexService = dexService;
	}

	public JarFile upload(MultipartFile file) {

        Objects.requireNonNull(file, "file cannot be null");

        String fileName = file.getOriginalFilename().toLowerCase();
        String name = fileName.substring(0, fileName.length() - 4);

		String jarName = uploaderService.upload(file);

		JarFile jarFile = new JarFile(name, jarName);
        String dexFileName = jarFile.getName() + ".dex";

        dexService.generateDexFile(filesDirectory + jarFile.getJar(), filesDirectory + dexFileName);

        jarFile.setDex(dexFileName);
        jarFileRepository.save(jarFile);

		return jarFile;
	}

	public Optional<JarFile> getFileByName(String fileName) {

        Objects.requireNonNull(fileName, "fileName cannot be null");
	    return jarFileRepository.findByName(fileName);
    }

	public InputStream getFileContentByFormat(JarFile file, FileFormat format) {

        Objects.requireNonNull(file, "file cannot be null");
        Objects.requireNonNull(format, "format cannot be null");

        String fileName = (format == FileFormat.DEX) ? file.getDex() : file.getJar();

        return uploaderService.getFileContent(fileName);
    }

	public File getFileByFormat(JarFile jarFile, FileFormat format) {

        Objects.requireNonNull(jarFile, "jarFile cannot be null");
        Objects.requireNonNull(format, "format cannot be null");

        String fileName = (format == FileFormat.DEX) ? jarFile.getDex() : jarFile.getJar();

        return new File(filesDirectory + fileName);
    }
}
