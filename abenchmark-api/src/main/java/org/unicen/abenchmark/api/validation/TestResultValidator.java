package org.unicen.abenchmark.api.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.result.TestResult;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestConfig;
import org.unicen.abenchmark.api.model.test.TestStatus;
import org.unicen.abenchmark.api.repository.TestRepository;
import org.unicen.abenchmark.api.repository.TestResultRepository;

import java.util.Objects;
import java.util.Optional;

@Component
public class TestResultValidator implements Validator {

    private final TestRepository testRepository;
    private final TestResultRepository testResultRepository;

    @Autowired
    public TestResultValidator(TestRepository testRepository, TestResultRepository testResultRepository) {

        Objects.requireNonNull(testRepository, "testRepository cannot be null");
        Objects.requireNonNull(testResultRepository, "testResultRepository cannot be null");

        this.testRepository = testRepository;
        this.testResultRepository = testResultRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TestResult.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        TestResult testResult = (TestResult) obj;
        String testId = testResult.getTestId();

        if(testId != null) {

            Test test = testRepository.findOne(testId);
            if(test == null) {
                errors.rejectValue("testId", "testId.notFound");
            }
            else {

                TestStatus testStatus = test.getStatus();
                if(testStatus == TestStatus.DONE) {
                    errors.rejectValue("testId", "testId.alreadyExistsResult");
                }
                else {
                    Optional<TestResult> existentResult = testResultRepository.findOneByTestId(testId);
                    existentResult.ifPresent(result -> errors.rejectValue("testId", "testId.alreadyExistsResult"));
                }
            }
        }

    }

}