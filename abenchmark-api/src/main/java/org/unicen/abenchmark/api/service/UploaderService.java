package org.unicen.abenchmark.api.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface UploaderService {

	String upload(MultipartFile file);

	InputStream getFileContent(String id);
}
