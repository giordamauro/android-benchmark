package org.unicen.abenchmark.api.service.upload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.unicen.abenchmark.api.config.AwsConfiguration;
import org.unicen.abenchmark.api.service.UploaderService;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

@Component
@ConditionalOnProperty(prefix = "uploader.service", name = "type", havingValue = "s3-resource")
public class S3ResourceUploader implements UploaderService {

    private final String awsBucket;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    public S3ResourceUploader(AwsConfiguration awsConfiguration) {

        Objects.requireNonNull(awsConfiguration, "awsConfiguration cannot be null");
        this.awsBucket = awsConfiguration.getBucketName();
    }

    @Override
    public String upload(MultipartFile file) {

        Objects.requireNonNull(file, "file cannot be null");
        String fileNameInS3 = file.getOriginalFilename();

        String resourceUrl = "s3://" + awsBucket + "/" + fileNameInS3;

        Resource resource = this.resourceLoader.getResource(resourceUrl);
        WritableResource writableResource = (WritableResource) resource;
        try (OutputStream outputStream = writableResource.getOutputStream()) {
            outputStream.write(file.getBytes());
            outputStream.flush();
        } catch (Exception e) {
            throw new IllegalStateException("Exception uploading file to S3 - Name: " + fileNameInS3, e);
        }

        return resourceUrl;
    }

    @Override
    public InputStream getFileContent(String id) {
        throw new UnsupportedOperationException("Not done yet");
    }
}
