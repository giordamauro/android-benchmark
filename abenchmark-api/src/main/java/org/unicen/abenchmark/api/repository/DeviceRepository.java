package org.unicen.abenchmark.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.unicen.abenchmark.api.model.device.Device;

import java.util.Optional;

public interface DeviceRepository extends PagingAndSortingRepository<Device, String> {

    Optional<Device> findByDeviceUuid(@Param("deviceUuid") String deviceUuid);
}