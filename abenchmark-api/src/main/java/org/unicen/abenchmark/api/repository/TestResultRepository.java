package org.unicen.abenchmark.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.unicen.abenchmark.api.model.result.TestResult;

import java.util.Optional;

public interface TestResultRepository extends PagingAndSortingRepository<TestResult, String> {

    Optional<TestResult> findOneByTestId(@Param("testId") String testId);
}