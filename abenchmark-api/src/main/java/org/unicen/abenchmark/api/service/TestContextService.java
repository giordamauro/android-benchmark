package org.unicen.abenchmark.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unicen.abenchmark.api.model.file.FileFormat;
import org.unicen.abenchmark.api.model.test.TestConfig;
import org.unicen.abenchmark.context.engine.BeanContext;
import org.unicen.abenchmark.context.model.Bean;
import org.unicen.abenchmark.context.support.JarFileClassLoader;
import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.operation.OperationListener;
import org.unicen.abenchmark.core.test.RunnerEvaluator;
import org.unicen.abenchmark.core.test.TestListener;
import org.unicen.abenchmark.core.test.TestRunner;
import org.unicen.abenchmark.core.test.TestRunnerBuilder;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TestContextService {

    private final JarFileService jarFileService;

    @Autowired
    public TestContextService(JarFileService jarFileService) {

        Objects.requireNonNull(jarFileService, "jarFileService cannot be null");

        this.jarFileService = jarFileService;
    }

    public List<String> getSourcePathUrls(List<String> sources) {

        Objects.requireNonNull(sources, "sources cannot be null");

        return sources.stream()
                .map(this::getPathUrlFromSource)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    public Optional<String> getPathUrlFromSource(String sourceName) {

        Objects.requireNonNull(sourceName, "sourceName cannot be null");

        return jarFileService.getFileByName(sourceName)
                .map(jarFile -> jarFileService.getFileByFormat(jarFile, FileFormat.JAR))
                .filter(File::exists)
                .map(File::getAbsolutePath);
    }

    public ClassLoader getJarUrlClassLoader(List<String> jarUrls) {

        Objects.requireNonNull(jarUrls, "jarUrls cannot be null");

        return JarFileClassLoader.builder()
                .parentClassLoader(this.getClass().getClassLoader())
                .filePaths(jarUrls)
                .build();
    }

    public TestRunner getTestRunnerFromTestConfig(BeanContext beanContext, TestConfig testConfig) {

        Objects.requireNonNull(testConfig, "testConfig cannot be null");

        TestRunnerBuilder runnerBuilder = TestRunner.builder();
        runnerBuilder.warmUpIterations(testConfig.getWarmUpIterations());

        Operation operation =  beanContext.createBean(testConfig.getOperation());
        runnerBuilder.operation(operation);

        RunnerEvaluator evaluator = beanContext.createBean(testConfig.getEvaluator());
        runnerBuilder.evaluator(evaluator);

        List<Bean> operationListenerBeans = testConfig.getOperationListeners();
        if (operationListenerBeans != null) {
            for (Bean operationListenerBean : operationListenerBeans) {

                OperationListener operationListener = beanContext.createBean(operationListenerBean);
                runnerBuilder.withOperationListener(operationListener);
            }
        }

        List<Bean> testListenerBeans = testConfig.getTestListeners();
        if (testListenerBeans != null) {
            for (Bean testListenerBean : testListenerBeans) {

                TestListener testListener = beanContext.createBean(testListenerBean);
                runnerBuilder.withTestListener(testListener);
            }
        }

        return runnerBuilder.build();
    }
}