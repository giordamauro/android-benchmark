package org.unicen.abenchmark.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestStatus;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface TestRepository extends PagingAndSortingRepository<Test, String> {

    List<Test> findByDeviceUuidAndStatus(@Param("deviceUuid") String deviceUuid, @Param("status") TestStatus status);
}