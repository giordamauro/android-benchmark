package org.unicen.abenchmark.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.validation.Validator;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.validation.TestResultValidator;
import org.unicen.abenchmark.api.validation.TestValidator;

@Configuration
public class RepositoryRestConfigurer extends RepositoryRestConfigurerAdapter {

    @Autowired
    @Qualifier("mvcValidator")
    private Validator validator;

    @Autowired
    private TestValidator testValidator;

    @Autowired
    private TestResultValidator testResultValidator;


    @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {

        validatingListener.addValidator("beforeCreate", validator);
        validatingListener.addValidator("beforeCreate", testValidator);
        validatingListener.addValidator("beforeCreate", testResultValidator);

        validatingListener.addValidator("beforeSave", validator);
        validatingListener.addValidator("beforeSave", testValidator);
    }

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Test.class);
    }
}