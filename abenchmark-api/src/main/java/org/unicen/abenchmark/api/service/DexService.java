package org.unicen.abenchmark.api.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Created by mgiorda on 2/4/17.
 */
@Service
public class DexService {

    private static final String DEX_COMMAND = "%sdx --dex --output=%s %s";

    @Value("${dx.tool.dir}")
    private String dxToolDir;

    @Async
    public void generateDexFileAsync(String origin, String output) {
        this.generateDexFile(origin, output);
    }

    public void generateDexFile(String origin, String output) {

        Objects.requireNonNull(origin, "origin cannot be null");
        Objects.requireNonNull(output, "output cannot be null");

        String command = String.format(DEX_COMMAND, dxToolDir, output, origin);

        try {
            Process process = Runtime.getRuntime().exec(new String[]{"bash", "-c", command});
            process.waitFor();

        } catch (Exception e) {
            throw new IllegalStateException("Exception generating Dex file with command " + command, e);
        }
    }
}
