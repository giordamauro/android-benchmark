package org.unicen.abenchmark.api.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.unicen.abenchmark.annotation.loader.TestAnnotationLoader;
import org.unicen.abenchmark.api.model.device.Device;
import org.unicen.abenchmark.api.model.file.FileFormat;
import org.unicen.abenchmark.api.model.test.Test;
import org.unicen.abenchmark.api.model.test.TestConfig;
import org.unicen.abenchmark.api.repository.DeviceRepository;
import org.unicen.abenchmark.api.service.JarFileService;
import org.unicen.abenchmark.api.service.TestContextService;
import org.unicen.abenchmark.context.engine.BeanContext;
import org.unicen.abenchmark.context.support.JarFileClassLoader;
import org.unicen.abenchmark.core.operation.Operation;
import org.unicen.abenchmark.core.test.TestRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class TestValidator implements Validator {

    private static final Logger log = LoggerFactory.getLogger(TestValidator.class);

    private final DeviceRepository deviceRepository;

    private final TestContextService testContextService;
    private final TestAnnotationLoader testAnnotationLoader;

    @Autowired
    public TestValidator(DeviceRepository deviceRepository, TestContextService testContextService) {

        Objects.requireNonNull(deviceRepository, "deviceRepository cannot be null");
        Objects.requireNonNull(testContextService, "testContextService cannot be null");

        this.deviceRepository = deviceRepository;

        this.testContextService = testContextService;
        this.testAnnotationLoader = TestAnnotationLoader.getInstance();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Test.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        Test test = (Test) obj;

        String deviceUuid = test.getDeviceUuid();
        if (deviceUuid != null) {
            this.validateDeviceUuid(deviceUuid, errors);
        }

        ClassLoader classLoader = this.getClass().getClassLoader();
        List<TestRunner> testRunners = new ArrayList<>();

        List<String> sources = test.getSources();
        if (sources != null) {
            List<String> jarUrls = this.validateSources(sources, errors);

            classLoader = testContextService.getJarUrlClassLoader(jarUrls);

            List<TestRunner> annotationTests = this.validateAnnotationSources(jarUrls, classLoader, errors);
            testRunners.addAll(annotationTests);
        }

        List<TestConfig> testConfigs = test.getTestConfigs();
        if(testConfigs != null) {

            List<TestRunner> configTests = this.validateTestConfigs(testConfigs, classLoader, errors);
            testRunners.addAll(configTests);
        }

        if(testRunners.isEmpty()) {
            errors.rejectValue("sources", "sources.noTests");
        }
    }

    public void validateDeviceUuid(String deviceUuid, Errors errors) {

        Optional<Device> device = deviceRepository.findByDeviceUuid(deviceUuid);
        if (!device.isPresent()) {
            errors.rejectValue("deviceUuid", "deviceUuid.notFound");
        }
    }

    public List<String> validateSources(List<String> sources, Errors errors) {

        List<String> jarUrls = new ArrayList<>();

        sources.forEach(sourceName -> {

            Optional<String> includeUrl = testContextService.getPathUrlFromSource(sourceName);

            if (includeUrl.isPresent()) {
                jarUrls.add(includeUrl.get());
            } else {
                log.warn("Exception on context declaration - Source file {} not found", sourceName);
                errors.rejectValue("testConfig", "testConfig.includeException");
            }
        });

        return jarUrls;
    }

    public List<TestRunner> validateAnnotationSources(List<String> jarUrls, ClassLoader classLoader, Errors errors) {

        try {
            List<Class<Operation>> benchmarkClasses = testAnnotationLoader.getBenchmarkClasses(classLoader, jarUrls);
            return testAnnotationLoader.loadAnnotationTests(classLoader, benchmarkClasses);

        } catch (Exception e) {
            log.warn("Exception on context declaration", e);
            errors.rejectValue("sources", "sources.contextException");
        }

        return new ArrayList<>();
    }

    public List<TestRunner> validateTestConfigs(List<TestConfig> testConfigs, ClassLoader classLoader, Errors errors) {

        List<TestRunner> testRunners = new ArrayList<>();

        try {
            BeanContext beanContext = new BeanContext.Builder()
                    .setClassLoader(classLoader)
                    .build();

            testConfigs.forEach(testConfig -> {

                TestRunner testRunner = testContextService.getTestRunnerFromTestConfig(beanContext, testConfig);
                testRunners.add(testRunner);
            });

        } catch (Exception e) {

            log.warn("Exception on context declaration", e);
            errors.rejectValue("testConfig", "testConfig.contextException");
        }

        return testRunners;
    }
}